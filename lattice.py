"""
Python class file containing the lattice class. Lattice is the building block for the stochastic simulation.
It contains attributes including:
    n_columns - the number of rows of the lattice
    n_rows - the number of columns of the lattice
    l - the thickness of each lattice point in microns
    species - a list containing species names which will be refered to by their index
    state_square - a 2d numpy array containing the state of the lattice--easy to read, but must update manually since not used in simulation
    state_interfaces - a 1d numpy array  containing interface type at each position
    reaction - a subclass containing reaction stoich, propensity, orientation
    advance - a method that performs a single lattice advance step
    interfaces - the 1D representation of lattice that shows the number of each type of interface
"""
import numpy as np
import pdb

class lattice:
    
    def __init__(self,
                 n_rows = 10,
                 n_columns = 10,
                 species = ['V', 'A', 'B', 'C', 'D', 'E']):
        import scipy.misc as spm

        self.n_rows = n_rows
        self.n_columns = n_columns
        if bool(n_columns % 2):
            raise Exception('for proper indexing, set number of columns to even number')
        self.reactions = [] # add reactions using the add_reaction method
        self.state_square = np.zeros((n_rows,n_columns),dtype=np.int) # fill lattice to initialize using fill_lattice method
        self.species = species
        self.N_spec = len(species)
        num_interface_types = 2*spm.comb(len(species)+1,2,exact = True) # Order matters for interface types
        self.state_interfaces = np.zeros(2*n_rows*n_columns-n_columns, dtype =np.int)
        self.interface_counts = np.bincount(self.state_interfaces,minlength=self.N_spec**2) 
        self.time = 0.

    def manual_initial_condition(self, state_square):
        """
        Input a full matrix as an initial condition manually
        """
        if self.state_square.size != state_square.size:
            raise Exception('Matrix is the wrong size')
        if (state_square >= len(self.species)).any():
            raise Exception('Species out of range')
        #pdb.set_trace()
        self.state_square = state_square
        self.state_interfaces_from_square()
        self.interface_counts = np.bincount(self.state_interfaces,minlength=self.N_spec**2) 

    def state_interfaces_from_square(self):
        """
        Get state_interfaces (1D, better for performance)
        from state_square (2D, easier to interpret)
        """
        position_count=0
        for row_index, row in enumerate(self.state_square):
            for column_index, value in enumerate(row):
                if row_index ==0: # skip row-adjacent interface if row_index = 0; unallowable interface
                    # do column-adjacent interface here
                    if column_index != 0: # if column index = zero, we need to wrap to other side
                        self.state_interfaces[position_count] = (
                            np.ravel_multi_index((value,row[column_index-1]),
                            (self.N_spec,self.N_spec)))
                        position_count+=1
                    else:
                        self.state_interfaces[position_count] = (
                            np.ravel_multi_index((value,row[-1]),
                            (self.N_spec,self.N_spec)))
                        position_count+=1

                else:
                    # do column-adjacent interface here
                    if column_index != 0: # if column index = zero, we need to wrap to other side
                        self.state_interfaces[position_count] = (
                            np.ravel_multi_index((value,row[column_index-1]),
                            (self.N_spec,self.N_spec)))
                        position_count+=1
                    else:
                        self.state_interfaces[position_count] = (
                            np.ravel_multi_index((value,row[-1]),
                            (self.N_spec,self.N_spec)))
                        position_count+=1
                    # do row-adjacent interface here
                    self.state_interfaces[position_count] = (
                            np.ravel_multi_index((value,self.state_square[row_index-1,column_index]),
                            (self.N_spec,self.N_spec)))
                    position_count+=1
                #print row_index, column_index, position_count


    def state_square_from_interfaces(self):
        """
        Get state_square (2D, easier to interpret)
        from state_interfaces (1D, better for performance)
        """
        # can fully reconstruct from every other column adjacent (i.e. even 1st row indices, then every 4th index after that) entries
        column_adj_counter = 0
        for i in (range(0,len(self.state_square.T),2)+range(len(self.state_square.T),len(self.state_interfaces),4)):
                position1 = np.unravel_index(column_adj_counter,(self.n_rows,self.n_columns))
                position2 = tuple(map(lambda x, y: x + y, position1, (0,-1)))
                if position2[1] < 0: # correct if we're at an edge case
                    position2 = tuple(map(lambda x,y:x+y,position2,(0,self.n_columns)))
                species1 = np.unravel_index(self.state_interfaces[i], (len(self.species),len(self.species)))[0]
                species2 = np.unravel_index(self.state_interfaces[i], (len(self.species),len(self.species)))[1]
                #pdb.set_trace()
                #print i,column_adj_counter
                self.state_square[position1[0],position1[1]] = species1
                self.state_square[position2[0],position2[1]] = species2
                column_adj_counter += 2



    def SSA_step(self,fast_diff=False,fullcount=False, update_square=False):
        """
        performs a single stochastic simulation algorithm step
        updating  the state_interfaces based on the reaction and diffusion propensities.
        to update state_square, run the separate state_square function--but only do this as needed to reduce computation time.
        fullcount, if true, will redo the full interface counting--better to adjust dynamically so False is default, but there for debugging
        update_square, if true, runs state_square_from_interfaces() at end of each step
        fast_diff sets the diffusion time to 0; not fully worked out method yet, use with caution
        """
        # get total reaction propensity for each reaction (P = p_constant*N_interfaces)
        props = np.zeros(len(self.reactions))#reaction props (remember diffusions are added (as reaction classes) with lattice.set_diffusion)
        if fullcount:
            temp = self.interface_counts.copy()
            self.interface_counts = np.bincount(self.state_interfaces,minlength=self.N_spec**2) 
            #print self.interface_counts-temp
        for i, reaction in enumerate(self.reactions):
           if reaction.inter1 != reaction.inter2: # don't double count if inter1 == inter2
               props[i] = (self.interface_counts[reaction.inter1]+
                           self.interface_counts[reaction.inter2])*reaction.propensity
           else:
               #pdb.set_trace()
               props[i] = self.interface_counts[reaction.inter1]*reaction.propensity

        if props.sum() == 0: # reaction has gone to completion (if possible)
            #print '0 propensity'
            return 
        # This gives us the reaction of interest
        reaction = self.reactions[np.nonzero(np.random.rand()<np.cumsum(props/props.sum()))[0][0]]
        # remove props for oriented if fast_diff is on
        if fast_diff:
            for i, r in enumerate(self.reactions):
                if r.orientation:
                    props[i] = 0.
        # This is our tau (zero for diffusion/ads/des for fast diff)
        if reaction.orientation and fast_diff:
            self.time += 0.
            #pdb.set_trace()
        else:
            self.time += np.log(1./np.random.rand())/props.sum()
            #print
            #pdb.set_trace()

        # Find (randomly) which of the possible reaction interfaces will be chosen
        if reaction.inter1!=reaction.inter2: # don't double count identical interfaces
            reactive_interface_number = np.random.randint(self.interface_counts[reaction.inter1]+
                                                      self.interface_counts[reaction.inter2])
        else:
            reactive_interface_number = np.random.randint(self.interface_counts[reaction.inter1])
        rxn_pos = np.where(np.logical_or(self.state_interfaces == reaction.inter1, self.state_interfaces == reaction.inter2))[0][reactive_interface_number]

        #pdb.set_trace()
        #print reaction.orientation
        if reaction.orientation: #Here, we change state interfaces and interface counts to products at the REACTION SITE -- depends on orientation
            if self.state_interfaces[rxn_pos] == reaction.inter1:
                self.state_interfaces[rxn_pos] = reaction.pinter1 # Change reaction site interface
                self.interface_counts[reaction.pinter1] += 1 # adjust counts for reaction site interfaces
                self.interface_counts[reaction.inter1] -= 1
            else:
                self.state_interfaces[rxn_pos] = reaction.pinter2
                self.interface_counts[reaction.pinter2] += 1
                self.interface_counts[reaction.inter2] -= 1

        else:
            if np.random.randint(2) == 0:
                self.interface_counts[self.state_interfaces[rxn_pos]] -= 1
                self.state_interfaces[rxn_pos] = reaction.pinter1
                self.interface_counts[reaction.pinter1] += 1
            else:
                self.interface_counts[self.state_interfaces[rxn_pos]] -= 1
                self.state_interfaces[rxn_pos] = reaction.pinter2
                self.interface_counts[reaction.pinter2] += 1
        #print rxn_pos, self.species[reaction.R1], self.species[reaction.R2], self.species[reaction.P1], self.species[reaction.P2]
        # Here we change interfaces on ADJACENT SITES; I.E., CHANGE ADJACENT INTERFACES
        # Adjacent sites depend on interface position--many possibilities EACH GIVEN IN AN IF/ELIF BELOW
        # IFF INTERFACE POSITION = ZERO
        if rxn_pos == 0:
            #print 'Pos = 0'
            # remove old interfaces from the count:
            self.interface_counts[self.state_interfaces[1]] += -1
            self.interface_counts[self.state_interfaces[self.n_columns-1]] += -1
            self.interface_counts[self.state_interfaces[self.n_columns+1]] += -1
            self.interface_counts[self.state_interfaces[3*self.n_columns-1]] += -1
            # update the interfaces
            self.state_interfaces[1] = np.ravel_multi_index((np.unravel_index(self.state_interfaces[1], (self.N_spec,self.N_spec))[0],
                                                             np.unravel_index(self.state_interfaces[0], (self.N_spec,self.N_spec))[0]),
                                                             (self.N_spec,self.N_spec))
            self.state_interfaces[self.n_columns-1] = np.ravel_multi_index((np.unravel_index(self.state_interfaces[0               ], (self.N_spec,self.N_spec))[1],
                                                                            np.unravel_index(self.state_interfaces[self.n_columns-1], (self.N_spec,self.N_spec))[1]),
                                                                            (self.N_spec,self.N_spec))
            self.state_interfaces[self.n_columns+1] = np.ravel_multi_index((np.unravel_index(self.state_interfaces[self.n_columns+1], (self.N_spec,self.N_spec))[0],
                                                                            np.unravel_index(self.state_interfaces[0               ], (self.N_spec,self.N_spec))[0]),
                                                                            (self.N_spec,self.N_spec))
            self.state_interfaces[3*self.n_columns-1] = np.ravel_multi_index((np.unravel_index(self.state_interfaces[3*self.n_columns-1], (self.N_spec,self.N_spec))[0],
                                                                              np.unravel_index(self.state_interfaces[0                 ], (self.N_spec,self.N_spec))[1]),
                                                                              (self.N_spec,self.N_spec))
            # add new interfaces from the count:
            self.interface_counts[self.state_interfaces[1]] += 1
            self.interface_counts[self.state_interfaces[self.n_columns-1]] += 1
            self.interface_counts[self.state_interfaces[self.n_columns+1]] += 1
            self.interface_counts[self.state_interfaces[3*self.n_columns-1]] += 1
        elif rxn_pos < self.n_columns-1: # IFF TOP ROW MIDDLE
            #print 'top row middle'
            # remove old interfaces from the count:
            self.interface_counts[self.state_interfaces[rxn_pos-1]] += -1
            self.interface_counts[self.state_interfaces[rxn_pos+1]] += -1
            self.interface_counts[self.state_interfaces[self.n_columns+2*rxn_pos+1]] += -1
            self.interface_counts[self.state_interfaces[self.n_columns+2*rxn_pos-1]] += -1
            # update the interfaces
            self.state_interfaces[rxn_pos-1] = np.ravel_multi_index((np.unravel_index(self.state_interfaces[rxn_pos  ], (self.N_spec,self.N_spec))[1],
                                                                     np.unravel_index(self.state_interfaces[rxn_pos-1], (self.N_spec,self.N_spec))[1]),
                                                                     (self.N_spec,self.N_spec))
            self.state_interfaces[rxn_pos+1] = np.ravel_multi_index((np.unravel_index(self.state_interfaces[rxn_pos+1], (self.N_spec,self.N_spec))[0],
                                                                     np.unravel_index(self.state_interfaces[rxn_pos  ], (self.N_spec,self.N_spec))[0]),
                                                                     (self.N_spec,self.N_spec))
            self.state_interfaces[self.n_columns+2*rxn_pos+1] = np.ravel_multi_index((np.unravel_index(self.state_interfaces[self.n_columns+2*rxn_pos+1], (self.N_spec,self.N_spec))[0],
                                                                                      np.unravel_index(self.state_interfaces[rxn_pos            ], (self.N_spec,self.N_spec))[0]),
                                                                                      (self.N_spec,self.N_spec))
            self.state_interfaces[self.n_columns+2*rxn_pos-1] = np.ravel_multi_index((np.unravel_index(self.state_interfaces[self.n_columns+2*rxn_pos-1], (self.N_spec,self.N_spec))[0],
                                                                                      np.unravel_index(self.state_interfaces[rxn_pos            ], (self.N_spec,self.N_spec))[1]),
                                                                                     (self.N_spec,self.N_spec))
            # add new interfaces from the count:
            self.interface_counts[self.state_interfaces[rxn_pos-1]] += 1
            self.interface_counts[self.state_interfaces[rxn_pos+1]] += 1
            self.interface_counts[self.state_interfaces[self.n_columns+2*rxn_pos+1]] += 1
            self.interface_counts[self.state_interfaces[self.n_columns+2*rxn_pos-1]] += 1
        elif rxn_pos == self.n_columns-1: # IFF TOP ROW RIGHT EDGE
            #print 'top right edge'
            # remove old interfaces from the count:
            self.interface_counts[self.state_interfaces[rxn_pos-1]] += -1
            self.interface_counts[self.state_interfaces[0]] += -1
            self.interface_counts[self.state_interfaces[self.n_columns+2*rxn_pos+1]] += -1
            self.interface_counts[self.state_interfaces[self.n_columns+2*rxn_pos-1]] += -1
            # update the interfaces
            self.state_interfaces[rxn_pos-1] = np.ravel_multi_index((np.unravel_index(self.state_interfaces[rxn_pos  ], (self.N_spec,self.N_spec))[1],
                                                                     np.unravel_index(self.state_interfaces[rxn_pos-1], (self.N_spec,self.N_spec))[1]),
                                                                     (self.N_spec,self.N_spec))
            self.state_interfaces[0] = np.ravel_multi_index((np.unravel_index(self.state_interfaces[0        ], (self.N_spec,self.N_spec))[0],
                                                             np.unravel_index(self.state_interfaces[rxn_pos  ], (self.N_spec,self.N_spec))[0]),
                                                             (self.N_spec,self.N_spec))
            self.state_interfaces[self.n_columns+2*rxn_pos+1] = np.ravel_multi_index((np.unravel_index(self.state_interfaces[self.n_columns+2*rxn_pos+1], (self.N_spec,self.N_spec))[0],
                                                                                      np.unravel_index(self.state_interfaces[rxn_pos            ], (self.N_spec,self.N_spec))[0]),
                                                                                     (self.N_spec,self.N_spec))
            self.state_interfaces[self.n_columns+2*rxn_pos-1] = np.ravel_multi_index((np.unravel_index(self.state_interfaces[self.n_columns+2*rxn_pos-1], (self.N_spec,self.N_spec))[0],
                                                                                      np.unravel_index(self.state_interfaces[rxn_pos            ], (self.N_spec,self.N_spec))[1]),
                                                                                      (self.N_spec,self.N_spec))
            # add new interfaces from the count:
            self.interface_counts[self.state_interfaces[rxn_pos-1]] += 1
            self.interface_counts[self.state_interfaces[0]] += 1
            self.interface_counts[self.state_interfaces[self.n_columns+2*rxn_pos+1]] += 1
            self.interface_counts[self.state_interfaces[self.n_columns+2*rxn_pos-1]] += 1
            
        elif rxn_pos & 0x1 and rxn_pos < 3*self.n_columns-1:# IFF 2nd ROW AND ODD BUT NOT RIGHT EDGE
            #print '2nd row odd (not right edge)'
            # remove old interfaces from the count:
            self.interface_counts[self.state_interfaces[(rxn_pos-self.n_columns-1)/2]] += -1
            self.interface_counts[self.state_interfaces[1+(rxn_pos-self.n_columns-1)/2]] += -1
            self.interface_counts[self.state_interfaces[rxn_pos-1]] += -1
            self.interface_counts[self.state_interfaces[rxn_pos+1]] += -1
            self.interface_counts[self.state_interfaces[rxn_pos+2*self.n_columns]] += -1
            # update the interfaces
            self.state_interfaces[(rxn_pos-self.n_columns-1)/2] = np.ravel_multi_index((np.unravel_index(self.state_interfaces[rxn_pos                     ], (self.N_spec,self.N_spec))[1],
                                                                                        np.unravel_index(self.state_interfaces[(rxn_pos-self.n_columns-1)/2], (self.N_spec,self.N_spec))[1]),
                                                                                        (self.N_spec,self.N_spec))
            self.state_interfaces[1+(rxn_pos-self.n_columns-1)/2] = np.ravel_multi_index((np.unravel_index(self.state_interfaces[1+(rxn_pos-self.n_columns-1)/2], (self.N_spec,self.N_spec))[0],
                                                                                          np.unravel_index(self.state_interfaces[rxn_pos                       ], (self.N_spec,self.N_spec))[1]),
                                                                                          (self.N_spec,self.N_spec))
            self.state_interfaces[rxn_pos-1] = np.ravel_multi_index((np.unravel_index(self.state_interfaces[rxn_pos  ], (self.N_spec,self.N_spec))[0],
                                                                     np.unravel_index(self.state_interfaces[rxn_pos-1], (self.N_spec,self.N_spec))[1]),
                                                                     (self.N_spec,self.N_spec))
            self.state_interfaces[rxn_pos+1] = np.ravel_multi_index((np.unravel_index(self.state_interfaces[rxn_pos+1], (self.N_spec,self.N_spec))[0],
                                                                     np.unravel_index(self.state_interfaces[rxn_pos  ], (self.N_spec,self.N_spec))[0]),
                                                                     (self.N_spec,self.N_spec))
            self.state_interfaces[rxn_pos+2*self.n_columns] = np.ravel_multi_index((np.unravel_index(self.state_interfaces[rxn_pos+2*self.n_columns], (self.N_spec,self.N_spec))[0],
                                                                                    np.unravel_index(self.state_interfaces[rxn_pos                 ], (self.N_spec,self.N_spec))[0]),
                                                                                    (self.N_spec,self.N_spec))
            # Add in the new interfaces to the count
            self.interface_counts[self.state_interfaces[(rxn_pos-self.n_columns-1)/2]] += 1
            self.interface_counts[self.state_interfaces[1+(rxn_pos-self.n_columns-1)/2]] += 1
            self.interface_counts[self.state_interfaces[rxn_pos-1]] += 1
            self.interface_counts[self.state_interfaces[rxn_pos+1]] += 1
            self.interface_counts[self.state_interfaces[rxn_pos+2*self.n_columns]] += 1
        elif rxn_pos == 3*self.n_columns-1: # IFF 2nd ROW RIGHT EDGE ODD
            #print '2nd row right edge odd'
            # remove old interfaces from the count:
            self.interface_counts[self.state_interfaces[self.n_columns-1]] += -1
            self.interface_counts[self.state_interfaces[0]] += -1
            self.interface_counts[self.state_interfaces[rxn_pos-1]] += -1
            self.interface_counts[self.state_interfaces[self.n_columns]] += -1
            self.interface_counts[self.state_interfaces[rxn_pos+2*self.n_columns]] += -1
            # update the interfaces
            self.state_interfaces[self.n_columns-1] = np.ravel_multi_index((np.unravel_index(self.state_interfaces[rxn_pos         ], (self.N_spec,self.N_spec))[1],
                                                                            np.unravel_index(self.state_interfaces[self.n_columns-1], (self.N_spec,self.N_spec))[1]),
                                                                            (self.N_spec,self.N_spec))
            self.state_interfaces[0] = np.ravel_multi_index((np.unravel_index(self.state_interfaces[0      ], (self.N_spec,self.N_spec))[0],
                                                             np.unravel_index(self.state_interfaces[rxn_pos], (self.N_spec,self.N_spec))[1]),
                                                             (self.N_spec,self.N_spec))
            self.state_interfaces[rxn_pos-1] = np.ravel_multi_index((np.unravel_index(self.state_interfaces[rxn_pos  ], (self.N_spec,self.N_spec))[0],
                                                                     np.unravel_index(self.state_interfaces[rxn_pos-1], (self.N_spec,self.N_spec))[1]),
                                                                     (self.N_spec,self.N_spec))
            self.state_interfaces[self.n_columns] = np.ravel_multi_index((np.unravel_index(self.state_interfaces[self.n_columns], (self.N_spec,self.N_spec))[0],
                                                                          np.unravel_index(self.state_interfaces[rxn_pos       ], (self.N_spec,self.N_spec))[0]),
                                                                         (self.N_spec,self.N_spec))
            self.state_interfaces[rxn_pos+2*self.n_columns] = np.ravel_multi_index((np.unravel_index(self.state_interfaces[rxn_pos+2*self.n_columns], (self.N_spec,self.N_spec))[0],
                                                                                    np.unravel_index(self.state_interfaces[rxn_pos                 ], (self.N_spec,self.N_spec))[0]),
                                                                                    (self.N_spec,self.N_spec))
            # add old interfaces to the count:
            self.interface_counts[self.state_interfaces[self.n_columns-1]] += 1
            self.interface_counts[self.state_interfaces[0]] += 1
            self.interface_counts[self.state_interfaces[rxn_pos-1]] += 1
            self.interface_counts[self.state_interfaces[self.n_columns]] += 1
            self.interface_counts[self.state_interfaces[rxn_pos+2*self.n_columns]] += 1
        elif rxn_pos == len(self.state_interfaces) - 2*self.n_columns: #IFF BOTTOM LEFT EVEN
            #print 'bottom left even'
            # remove old interfaces from the count:
            self.interface_counts[self.state_interfaces[rxn_pos+1]] += -1
            self.interface_counts[self.state_interfaces[rxn_pos+2]] += -1
            self.interface_counts[self.state_interfaces[len(self.state_interfaces)-1]] += -1
            self.interface_counts[self.state_interfaces[len(self.state_interfaces)-2]] += -1
            # Update interfaces
            self.state_interfaces[rxn_pos+1] = np.ravel_multi_index((np.unravel_index(self.state_interfaces[rxn_pos  ], (self.N_spec,self.N_spec))[0],
                                                                     np.unravel_index(self.state_interfaces[rxn_pos+1], (self.N_spec,self.N_spec))[1]),
                                                                     (self.N_spec,self.N_spec))
            self.state_interfaces[rxn_pos+2] = np.ravel_multi_index((np.unravel_index(self.state_interfaces[rxn_pos+2], (self.N_spec,self.N_spec))[0],
                                                                     np.unravel_index(self.state_interfaces[rxn_pos  ], (self.N_spec,self.N_spec))[0]),
                                                                     (self.N_spec,self.N_spec))
            self.state_interfaces[len(self.state_interfaces)-1] = np.ravel_multi_index((np.unravel_index(self.state_interfaces[rxn_pos                     ], (self.N_spec,self.N_spec))[1],
                                                                                        np.unravel_index(self.state_interfaces[len(self.state_interfaces)-1], (self.N_spec,self.N_spec))[1]),
                                                                                        (self.N_spec,self.N_spec))
            self.state_interfaces[len(self.state_interfaces)-2] = np.ravel_multi_index((np.unravel_index(self.state_interfaces[rxn_pos                     ], (self.N_spec,self.N_spec))[1],
                                                                                        np.unravel_index(self.state_interfaces[len(self.state_interfaces)-2], (self.N_spec,self.N_spec))[1]),
                                                                                        (self.N_spec,self.N_spec))
            # add old interfaces to the count:
            self.interface_counts[self.state_interfaces[rxn_pos+1]] += 1
            self.interface_counts[self.state_interfaces[rxn_pos+2]] += 1
            self.interface_counts[self.state_interfaces[len(self.state_interfaces)-1]] += 1
            self.interface_counts[self.state_interfaces[len(self.state_interfaces)-2]] += 1
        elif rxn_pos == len(self.state_interfaces)-2:# IFF BOTTOM RIGHT EVEN
            #print 'bottom right even'
            # remove old interfaces from the count:
            self.interface_counts[self.state_interfaces[rxn_pos+1]] += -1
            self.interface_counts[self.state_interfaces[rxn_pos-2*self.n_columns+2]] += -1
            self.interface_counts[self.state_interfaces[rxn_pos-1]] += -1
            self.interface_counts[self.state_interfaces[rxn_pos-2]] += -1
            # Update interfaces
            self.state_interfaces[rxn_pos+1] = np.ravel_multi_index((np.unravel_index(self.state_interfaces[rxn_pos  ], (self.N_spec,self.N_spec))[0],
                                                                     np.unravel_index(self.state_interfaces[rxn_pos+1], (self.N_spec,self.N_spec))[1]),
                                                                     (self.N_spec,self.N_spec))
            self.state_interfaces[rxn_pos-2*self.n_columns+2] = np.ravel_multi_index((np.unravel_index(self.state_interfaces[rxn_pos-2*self.n_columns+2], (self.N_spec,self.N_spec))[0],
                                                                                      np.unravel_index(self.state_interfaces[rxn_pos            ], (self.N_spec,self.N_spec))[0]),
                                                                                      (self.N_spec,self.N_spec))
            self.state_interfaces[rxn_pos-1] = np.ravel_multi_index((np.unravel_index(self.state_interfaces[rxn_pos  ], (self.N_spec,self.N_spec))[1],
                                                                     np.unravel_index(self.state_interfaces[rxn_pos-1], (self.N_spec,self.N_spec))[1]),
                                                                     (self.N_spec,self.N_spec))
            self.state_interfaces[rxn_pos-2] = np.ravel_multi_index((np.unravel_index(self.state_interfaces[rxn_pos  ], (self.N_spec,self.N_spec))[1],
                                                                     np.unravel_index(self.state_interfaces[rxn_pos-2], (self.N_spec,self.N_spec))[1]),
                                                                     (self.N_spec,self.N_spec))
            # add old interfaces to the count:
            self.interface_counts[self.state_interfaces[rxn_pos+1]] += 1
            self.interface_counts[self.state_interfaces[rxn_pos-2*self.n_columns+2]] += 1
            self.interface_counts[self.state_interfaces[rxn_pos-1]] += 1
            self.interface_counts[self.state_interfaces[rxn_pos-2]] += 1
        elif rxn_pos == len(self.state_interfaces)-1:# IFF BOTTOM RIGHT ODD
            #pdb.set_trace()
            #print 'bottom right odd'
            # remove old interfaces from the count:
            self.interface_counts[self.state_interfaces[rxn_pos-1]] += -1#
            self.interface_counts[self.state_interfaces[rxn_pos-2*self.n_columns+1]] += -1#
            self.interface_counts[self.state_interfaces[rxn_pos-2*self.n_columns]] += -1#
            self.interface_counts[self.state_interfaces[rxn_pos-2*self.n_columns-1]] += -1#
            self.interface_counts[self.state_interfaces[rxn_pos-4*self.n_columns+1]] += -1
            # Update interfaces
            self.state_interfaces[rxn_pos-1] = np.ravel_multi_index((np.unravel_index(self.state_interfaces[rxn_pos  ], (self.N_spec,self.N_spec))[0],
                                                                     np.unravel_index(self.state_interfaces[rxn_pos-1], (self.N_spec,self.N_spec))[1]),
                                                                     (self.N_spec,self.N_spec))
            self.state_interfaces[rxn_pos-2*self.n_columns+1] = np.ravel_multi_index((np.unravel_index(self.state_interfaces[rxn_pos-2*self.n_columns+1], (self.N_spec,self.N_spec))[0],
                                                                                      np.unravel_index(self.state_interfaces[rxn_pos                   ], (self.N_spec,self.N_spec))[0]),
                                                                                      (self.N_spec,self.N_spec))
            self.state_interfaces[rxn_pos-2*self.n_columns] = np.ravel_multi_index((np.unravel_index(self.state_interfaces[rxn_pos                 ], (self.N_spec,self.N_spec))[1],
                                                                                    np.unravel_index(self.state_interfaces[rxn_pos-2*self.n_columns], (self.N_spec,self.N_spec))[1]),
                                                                                    (self.N_spec,self.N_spec))
            self.state_interfaces[rxn_pos-2*self.n_columns-1] = np.ravel_multi_index((np.unravel_index(self.state_interfaces[rxn_pos                   ], (self.N_spec,self.N_spec))[1],
                                                                                      np.unravel_index(self.state_interfaces[rxn_pos-2*self.n_columns-1], (self.N_spec,self.N_spec))[1]),
                                                                                      (self.N_spec,self.N_spec))
            self.state_interfaces[rxn_pos-4*self.n_columns+1] = np.ravel_multi_index((np.unravel_index(self.state_interfaces[rxn_pos-4*self.n_columns+1], (self.N_spec,self.N_spec))[0],
                                                                                      np.unravel_index(self.state_interfaces[rxn_pos                   ], (self.N_spec,self.N_spec))[1]),
                                                                                      (self.N_spec,self.N_spec))
            # add old interfaces to the count:
            self.interface_counts[self.state_interfaces[rxn_pos-1]] += 1
            self.interface_counts[self.state_interfaces[rxn_pos-2*self.n_columns+1]] += 1
            self.interface_counts[self.state_interfaces[rxn_pos-2*self.n_columns]] += 1
            self.interface_counts[self.state_interfaces[rxn_pos-2*self.n_columns-1]] += 1
            self.interface_counts[self.state_interfaces[rxn_pos-4*self.n_columns+1]] += 1
        elif rxn_pos > len(self.state_interfaces)-2*self.n_columns and not rxn_pos & 0x1:# IFF BOTTOM MIDDLE EVEN
            #pdb.set_trace()
            #print 'bottom middle even'
            # remove old interfaces from the count:
            self.interface_counts[self.state_interfaces[rxn_pos+1]] += -1
            self.interface_counts[self.state_interfaces[rxn_pos+2]] += -1
            self.interface_counts[self.state_interfaces[rxn_pos-1]] += -1
            self.interface_counts[self.state_interfaces[rxn_pos-2]] += -1
            # Update interfaces
            self.state_interfaces[rxn_pos+1] = np.ravel_multi_index((np.unravel_index(self.state_interfaces[rxn_pos  ], (self.N_spec,self.N_spec))[0],
                                                                     np.unravel_index(self.state_interfaces[rxn_pos+1], (self.N_spec,self.N_spec))[1]),
                                                                     (self.N_spec,self.N_spec))
            self.state_interfaces[rxn_pos+2] = np.ravel_multi_index((np.unravel_index(self.state_interfaces[rxn_pos+2], (self.N_spec,self.N_spec))[0],
                                                                     np.unravel_index(self.state_interfaces[rxn_pos  ], (self.N_spec,self.N_spec))[0]),
                                                                     (self.N_spec,self.N_spec))
            self.state_interfaces[rxn_pos-1] = np.ravel_multi_index((np.unravel_index(self.state_interfaces[rxn_pos  ], (self.N_spec,self.N_spec))[1],
                                                                     np.unravel_index(self.state_interfaces[rxn_pos-1], (self.N_spec,self.N_spec))[1]),
                                                                     (self.N_spec,self.N_spec))
            self.state_interfaces[rxn_pos-2] = np.ravel_multi_index((np.unravel_index(self.state_interfaces[rxn_pos  ], (self.N_spec,self.N_spec))[1],
                                                                     np.unravel_index(self.state_interfaces[rxn_pos-2], (self.N_spec,self.N_spec))[1]),
                                                                     (self.N_spec,self.N_spec))
            # add old interfaces to the count:
            self.interface_counts[self.state_interfaces[rxn_pos+1]] += 1
            self.interface_counts[self.state_interfaces[rxn_pos+2]] += 1
            self.interface_counts[self.state_interfaces[rxn_pos-1]] += 1
            self.interface_counts[self.state_interfaces[rxn_pos-2]] += 1
        elif rxn_pos > len(self.state_interfaces)-2*self.n_columns and rxn_pos & 0x1:# IFF BOTTOM MIDDLE OR LEFT EDGE ODD
            #print 'bottom middle or left edge odd'
            # remove old interfaces from the count:
            self.interface_counts[self.state_interfaces[rxn_pos-1]] += -1
            self.interface_counts[self.state_interfaces[rxn_pos+1]] += -1
            self.interface_counts[self.state_interfaces[rxn_pos-2*self.n_columns]] += -1
            self.interface_counts[self.state_interfaces[rxn_pos-2*self.n_columns-1]] += -1
            self.interface_counts[self.state_interfaces[rxn_pos-2*self.n_columns+1]] += -1
            # Update interfaces
            self.state_interfaces[rxn_pos-1] = np.ravel_multi_index((np.unravel_index(self.state_interfaces[rxn_pos  ], (self.N_spec,self.N_spec))[0],
                                                                     np.unravel_index(self.state_interfaces[rxn_pos-1], (self.N_spec,self.N_spec))[1]),
                                                                     (self.N_spec,self.N_spec))
            self.state_interfaces[rxn_pos+1] = np.ravel_multi_index((np.unravel_index(self.state_interfaces[rxn_pos+1], (self.N_spec,self.N_spec))[0],
                                                                     np.unravel_index(self.state_interfaces[rxn_pos  ], (self.N_spec,self.N_spec))[0]),
                                                                     (self.N_spec,self.N_spec))
            self.state_interfaces[rxn_pos-2*self.n_columns] = np.ravel_multi_index((np.unravel_index(self.state_interfaces[rxn_pos  ], (self.N_spec,self.N_spec))[1],
                                                                             np.unravel_index(self.state_interfaces[rxn_pos-2*self.n_columns], (self.N_spec,self.N_spec))[1]),
                                                                             (self.N_spec,self.N_spec))
            self.state_interfaces[rxn_pos-2*self.n_columns-1] = np.ravel_multi_index((np.unravel_index(self.state_interfaces[rxn_pos  ], (self.N_spec,self.N_spec))[1],
                                                                               np.unravel_index(self.state_interfaces[rxn_pos-2*self.n_columns-1], (self.N_spec,self.N_spec))[1]),
                                                                               (self.N_spec,self.N_spec))
            self.state_interfaces[rxn_pos-2*self.n_columns+1] = np.ravel_multi_index((np.unravel_index(self.state_interfaces[rxn_pos-2*self.n_columns+1], (self.N_spec,self.N_spec))[0],
                                                                               np.unravel_index(self.state_interfaces[rxn_pos            ], (self.N_spec,self.N_spec))[1]),
                                                                               (self.N_spec,self.N_spec))
            # add old interfaces to the count:
            self.interface_counts[self.state_interfaces[rxn_pos-1]] += 1
            self.interface_counts[self.state_interfaces[rxn_pos+1]] += 1
            self.interface_counts[self.state_interfaces[rxn_pos-2*self.n_columns]] += 1
            self.interface_counts[self.state_interfaces[rxn_pos-2*self.n_columns-1]] += 1
            self.interface_counts[self.state_interfaces[rxn_pos-2*self.n_columns+1]] +=1

        elif np.mod(rxn_pos+self.n_columns, 2*self.n_columns)==0: # IFF MIDDLE LEFT EVEN
            #print 'middle left even'
            # remove old interfaces from the count:
            self.interface_counts[self.state_interfaces[rxn_pos+1]] += -1#
            self.interface_counts[self.state_interfaces[rxn_pos+2]] += -1#
            self.interface_counts[self.state_interfaces[rxn_pos+2*self.n_columns-1]] += -1#
            self.interface_counts[self.state_interfaces[rxn_pos+2*self.n_columns-2]] += -1#
            self.interface_counts[self.state_interfaces[rxn_pos+2*self.n_columns+1]] += -1
            self.interface_counts[self.state_interfaces[rxn_pos+4*self.n_columns-1]] += -1
            # Update interfaces
            self.state_interfaces[rxn_pos+1] = np.ravel_multi_index((np.unravel_index(self.state_interfaces[rxn_pos  ], (self.N_spec,self.N_spec))[0],
                                                                     np.unravel_index(self.state_interfaces[rxn_pos+1], (self.N_spec,self.N_spec))[1]),
                                                                     (self.N_spec,self.N_spec))
            self.state_interfaces[rxn_pos+2] = np.ravel_multi_index((np.unravel_index(self.state_interfaces[rxn_pos+2], (self.N_spec,self.N_spec))[0],
                                                                     np.unravel_index(self.state_interfaces[rxn_pos  ], (self.N_spec,self.N_spec))[0]),
                                                                     (self.N_spec,self.N_spec))
            self.state_interfaces[rxn_pos+2*self.n_columns-1] = np.ravel_multi_index((np.unravel_index(self.state_interfaces[rxn_pos            ], (self.N_spec,self.N_spec))[1],
                                                                                      np.unravel_index(self.state_interfaces[rxn_pos+2*self.n_columns-1], (self.N_spec,self.N_spec))[1]),
                                                                                      (self.N_spec,self.N_spec))
            self.state_interfaces[rxn_pos+2*self.n_columns-2] = np.ravel_multi_index((np.unravel_index(self.state_interfaces[rxn_pos            ], (self.N_spec,self.N_spec))[1],
                                                                                      np.unravel_index(self.state_interfaces[rxn_pos+2*self.n_columns-2], (self.N_spec,self.N_spec))[1]),
                                                                                      (self.N_spec,self.N_spec))
            self.state_interfaces[rxn_pos+2*self.n_columns+1] = np.ravel_multi_index((np.unravel_index(self.state_interfaces[rxn_pos+2*self.n_columns+1], (self.N_spec,self.N_spec))[0],
                                                                                      np.unravel_index(self.state_interfaces[rxn_pos            ], (self.N_spec,self.N_spec))[0]),
                                                                                      (self.N_spec,self.N_spec))
            self.state_interfaces[rxn_pos+4*self.n_columns-1] = np.ravel_multi_index((np.unravel_index(self.state_interfaces[rxn_pos+4*self.n_columns-1], (self.N_spec,self.N_spec))[0],
                                                                                      np.unravel_index(self.state_interfaces[rxn_pos            ], (self.N_spec,self.N_spec))[1]),
                                                                                      (self.N_spec,self.N_spec))
            # add old interfaces to the count:
            self.interface_counts[self.state_interfaces[rxn_pos+1]] += 1
            self.interface_counts[self.state_interfaces[rxn_pos+2]] += 1
            self.interface_counts[self.state_interfaces[rxn_pos+2*self.n_columns-1]] += 1
            self.interface_counts[self.state_interfaces[rxn_pos+2*self.n_columns-2]] += 1
            self.interface_counts[self.state_interfaces[rxn_pos+2*self.n_columns+1]] += 1
            self.interface_counts[self.state_interfaces[rxn_pos+4*self.n_columns-1]] += 1
        elif  np.mod(rxn_pos+2+self.n_columns, 2*self.n_columns)==0: # IFF MIDDLE RIGHT EVEN
            #print 'middle right even'
            # remove old interfaces from the count:
            self.interface_counts[self.state_interfaces[rxn_pos+1]] += -1
            self.interface_counts[self.state_interfaces[rxn_pos-2*self.n_columns+2]] += -1
            self.interface_counts[self.state_interfaces[rxn_pos-1]] += -1
            self.interface_counts[self.state_interfaces[rxn_pos-2]] += -1
            self.interface_counts[self.state_interfaces[rxn_pos+2*self.n_columns+1]] += -1
            self.interface_counts[self.state_interfaces[rxn_pos+2*self.n_columns-1]] += -1
            # Update interfaces
            self.state_interfaces[rxn_pos+1] = np.ravel_multi_index((np.unravel_index(self.state_interfaces[rxn_pos  ], (self.N_spec,self.N_spec))[0],
                                                                     np.unravel_index(self.state_interfaces[rxn_pos+1], (self.N_spec,self.N_spec))[1]),
                                                                     (self.N_spec,self.N_spec))
            self.state_interfaces[rxn_pos-2*self.n_columns+2] = np.ravel_multi_index((np.unravel_index(self.state_interfaces[rxn_pos-2*self.n_columns+2], (self.N_spec,self.N_spec))[0],
                                                                                      np.unravel_index(self.state_interfaces[rxn_pos                   ], (self.N_spec,self.N_spec))[0]),
                                                                                      (self.N_spec,self.N_spec))
            self.state_interfaces[rxn_pos-1] = np.ravel_multi_index((np.unravel_index(self.state_interfaces[rxn_pos  ], (self.N_spec,self.N_spec))[1],
                                                                     np.unravel_index(self.state_interfaces[rxn_pos-1], (self.N_spec,self.N_spec))[1]),
                                                                     (self.N_spec,self.N_spec))
            self.state_interfaces[rxn_pos-2] = np.ravel_multi_index((np.unravel_index(self.state_interfaces[rxn_pos  ], (self.N_spec,self.N_spec))[1],
                                                                     np.unravel_index(self.state_interfaces[rxn_pos-2], (self.N_spec,self.N_spec))[1]),
                                                                     (self.N_spec,self.N_spec))
            self.state_interfaces[rxn_pos+2*self.n_columns+1] = np.ravel_multi_index((np.unravel_index(self.state_interfaces[rxn_pos+2*self.n_columns+1], (self.N_spec,self.N_spec))[0],
                                                                                      np.unravel_index(self.state_interfaces[rxn_pos                   ], (self.N_spec,self.N_spec))[0]),
                                                                                      (self.N_spec,self.N_spec))
            self.state_interfaces[rxn_pos+2*self.n_columns-1] = np.ravel_multi_index((np.unravel_index(self.state_interfaces[rxn_pos+2*self.n_columns-1], (self.N_spec,self.N_spec))[0],
                                                                                      np.unravel_index(self.state_interfaces[rxn_pos                   ], (self.N_spec,self.N_spec))[1]),
                                                                                      (self.N_spec,self.N_spec))
            # add old interfaces to the count:
            self.interface_counts[self.state_interfaces[rxn_pos+1]] += 1
            self.interface_counts[self.state_interfaces[rxn_pos-2*self.n_columns+2]] += 1
            self.interface_counts[self.state_interfaces[rxn_pos-1]] += 1
            self.interface_counts[self.state_interfaces[rxn_pos-2]] += 1
            self.interface_counts[self.state_interfaces[rxn_pos+2*self.n_columns+1]] += 1
            self.interface_counts[self.state_interfaces[rxn_pos+2*self.n_columns-1]] += 1

        elif np.mod(rxn_pos+1+self.n_columns, 2*self.n_columns) == 0:# IFF MIDDLE RIGHT ODD
            #print 'middle right odd'
            # remove old interfaces from the count:
            self.interface_counts[self.state_interfaces[rxn_pos-1]] += -1#
            self.interface_counts[self.state_interfaces[rxn_pos-2*self.n_columns+1]] += -1#
            self.interface_counts[self.state_interfaces[rxn_pos-2*self.n_columns]] += -1#
            self.interface_counts[self.state_interfaces[rxn_pos-2*self.n_columns-1]] += -1#
            self.interface_counts[self.state_interfaces[rxn_pos-4*self.n_columns+1]] += -1
            self.interface_counts[self.state_interfaces[rxn_pos+2*self.n_columns]] += -1#
            # Update interfaces
            self.state_interfaces[rxn_pos-1] = np.ravel_multi_index((np.unravel_index(self.state_interfaces[rxn_pos  ], (self.N_spec,self.N_spec))[0],
                                                                     np.unravel_index(self.state_interfaces[rxn_pos-1], (self.N_spec,self.N_spec))[1]),
                                                                     (self.N_spec,self.N_spec))
            self.state_interfaces[rxn_pos-2*self.n_columns+1] = np.ravel_multi_index((np.unravel_index(self.state_interfaces[rxn_pos-2*self.n_columns+1], (self.N_spec,self.N_spec))[0],
                                                                                      np.unravel_index(self.state_interfaces[rxn_pos                   ], (self.N_spec,self.N_spec))[0]),
                                                                                      (self.N_spec,self.N_spec))
            self.state_interfaces[rxn_pos-2*self.n_columns] = np.ravel_multi_index((np.unravel_index(self.state_interfaces[rxn_pos                 ], (self.N_spec,self.N_spec))[1],
                                                                                    np.unravel_index(self.state_interfaces[rxn_pos-2*self.n_columns], (self.N_spec,self.N_spec))[1]),
                                                                                    (self.N_spec,self.N_spec))
            self.state_interfaces[rxn_pos-2*self.n_columns-1] = np.ravel_multi_index((np.unravel_index(self.state_interfaces[rxn_pos                   ], (self.N_spec,self.N_spec))[1],
                                                                                      np.unravel_index(self.state_interfaces[rxn_pos-2*self.n_columns-1], (self.N_spec,self.N_spec))[1]),
                                                                                      (self.N_spec,self.N_spec))
            self.state_interfaces[rxn_pos-4*self.n_columns+1] = np.ravel_multi_index((np.unravel_index(self.state_interfaces[rxn_pos-4*self.n_columns+1], (self.N_spec,self.N_spec))[0],
                                                                                      np.unravel_index(self.state_interfaces[rxn_pos                   ], (self.N_spec,self.N_spec))[1]),
                                                                                      (self.N_spec,self.N_spec))
            self.state_interfaces[rxn_pos+2*self.n_columns] = np.ravel_multi_index((np.unravel_index(self.state_interfaces[rxn_pos+2*self.n_columns], (self.N_spec,self.N_spec))[0],
                                                                                    np.unravel_index(self.state_interfaces[rxn_pos                 ], (self.N_spec,self.N_spec))[0]),
                                                                                    (self.N_spec,self.N_spec))
            # add old interfaces to the count:
            self.interface_counts[self.state_interfaces[rxn_pos-1]] += 1
            self.interface_counts[self.state_interfaces[rxn_pos-2*self.n_columns+1]] += 1
            self.interface_counts[self.state_interfaces[rxn_pos-2*self.n_columns]] += 1
            self.interface_counts[self.state_interfaces[rxn_pos-2*self.n_columns-1]] += 1
            self.interface_counts[self.state_interfaces[rxn_pos-4*self.n_columns+1]] += 1
            self.interface_counts[self.state_interfaces[rxn_pos+2*self.n_columns]] += 1
            
        elif rxn_pos & 0x1: # IFF MIDDLE ODD
            #print 'middle odd'
            # remove old interfaces from the count:
            self.interface_counts[self.state_interfaces[rxn_pos-1]] += -1
            self.interface_counts[self.state_interfaces[rxn_pos+1]] += -1
            self.interface_counts[self.state_interfaces[rxn_pos-2*self.n_columns]] += -1
            self.interface_counts[self.state_interfaces[rxn_pos-2*self.n_columns-1]] += -1
            self.interface_counts[self.state_interfaces[rxn_pos-2*self.n_columns+1]] += -1
            self.interface_counts[self.state_interfaces[rxn_pos+2*self.n_columns]] += -1
            # Update interfaces
            self.state_interfaces[rxn_pos-1] = np.ravel_multi_index((np.unravel_index(self.state_interfaces[rxn_pos  ], (self.N_spec,self.N_spec))[0],
                                                                     np.unravel_index(self.state_interfaces[rxn_pos-1], (self.N_spec,self.N_spec))[1]),
                                                                     (self.N_spec,self.N_spec))
            self.state_interfaces[rxn_pos+1] = np.ravel_multi_index((np.unravel_index(self.state_interfaces[rxn_pos+1], (self.N_spec,self.N_spec))[0],
                                                                     np.unravel_index(self.state_interfaces[rxn_pos  ], (self.N_spec,self.N_spec))[0]),
                                                                     (self.N_spec,self.N_spec))
            self.state_interfaces[rxn_pos-2*self.n_columns] = np.ravel_multi_index((np.unravel_index(self.state_interfaces[rxn_pos                 ], (self.N_spec,self.N_spec))[1],
                                                                                    np.unravel_index(self.state_interfaces[rxn_pos-2*self.n_columns], (self.N_spec,self.N_spec))[1]),
                                                                                    (self.N_spec,self.N_spec))
            self.state_interfaces[rxn_pos-2*self.n_columns-1] = np.ravel_multi_index((np.unravel_index(self.state_interfaces[rxn_pos                   ], (self.N_spec,self.N_spec))[1],
                                                                                      np.unravel_index(self.state_interfaces[rxn_pos-2*self.n_columns-1], (self.N_spec,self.N_spec))[1]),
                                                                                      (self.N_spec,self.N_spec))
            self.state_interfaces[rxn_pos-2*self.n_columns+1] = np.ravel_multi_index((np.unravel_index(self.state_interfaces[rxn_pos-2*self.n_columns+1], (self.N_spec,self.N_spec))[0],
                                                                                      np.unravel_index(self.state_interfaces[rxn_pos                   ], (self.N_spec,self.N_spec))[1]),
                                                                                     (self.N_spec,self.N_spec))
            self.state_interfaces[rxn_pos+2*self.n_columns] = np.ravel_multi_index((np.unravel_index(self.state_interfaces[rxn_pos+2*self.n_columns], (self.N_spec,self.N_spec))[0],
                                                                                    np.unravel_index(self.state_interfaces[rxn_pos                 ], (self.N_spec,self.N_spec))[0]),
                                                                                    (self.N_spec,self.N_spec))
            # add old interfaces to the count:
            self.interface_counts[self.state_interfaces[rxn_pos-1]] += 1
            self.interface_counts[self.state_interfaces[rxn_pos+1]] += 1
            self.interface_counts[self.state_interfaces[rxn_pos-2*self.n_columns]] += 1
            self.interface_counts[self.state_interfaces[rxn_pos-2*self.n_columns-1]] += 1
            self.interface_counts[self.state_interfaces[rxn_pos-2*self.n_columns+1]] +=1
            self.interface_counts[self.state_interfaces[rxn_pos+2*self.n_columns]] += 1

        elif not rxn_pos & 0x1: # IFF EVEN AND MIDDLE
            #print 'even middle'
            # remove old interfaces from the count:
            self.interface_counts[self.state_interfaces[rxn_pos+1]] += -1
            self.interface_counts[self.state_interfaces[rxn_pos+2]] += -1
            self.interface_counts[self.state_interfaces[rxn_pos-1]] += -1
            self.interface_counts[self.state_interfaces[rxn_pos-2]] += -1
            self.interface_counts[self.state_interfaces[rxn_pos+2*self.n_columns+1]] += -1
            self.interface_counts[self.state_interfaces[rxn_pos+2*self.n_columns-1]] += -1
            # Update interfaces
            self.state_interfaces[rxn_pos+1] = np.ravel_multi_index((np.unravel_index(self.state_interfaces[rxn_pos  ], (self.N_spec,self.N_spec))[0],
                                                                     np.unravel_index(self.state_interfaces[rxn_pos+1], (self.N_spec,self.N_spec))[1]),
                                                                     (self.N_spec,self.N_spec))
            self.state_interfaces[rxn_pos+2] = np.ravel_multi_index((np.unravel_index(self.state_interfaces[rxn_pos+2], (self.N_spec,self.N_spec))[0],
                                                                     np.unravel_index(self.state_interfaces[rxn_pos  ], (self.N_spec,self.N_spec))[0]),
                                                                     (self.N_spec,self.N_spec))
            self.state_interfaces[rxn_pos-1] = np.ravel_multi_index((np.unravel_index(self.state_interfaces[rxn_pos  ], (self.N_spec,self.N_spec))[1],
                                                                     np.unravel_index(self.state_interfaces[rxn_pos-1], (self.N_spec,self.N_spec))[1]),
                                                                     (self.N_spec,self.N_spec))
            self.state_interfaces[rxn_pos-2] = np.ravel_multi_index((np.unravel_index(self.state_interfaces[rxn_pos  ], (self.N_spec,self.N_spec))[1],
                                                                     np.unravel_index(self.state_interfaces[rxn_pos-2], (self.N_spec,self.N_spec))[1]),
                                                                     (self.N_spec,self.N_spec))
            self.state_interfaces[rxn_pos+2*self.n_columns+1] = np.ravel_multi_index((np.unravel_index(self.state_interfaces[rxn_pos+2*self.n_columns+1], (self.N_spec,self.N_spec))[0],
                                                                                      np.unravel_index(self.state_interfaces[rxn_pos                   ], (self.N_spec,self.N_spec))[0]),
                                                                                      (self.N_spec,self.N_spec))
            self.state_interfaces[rxn_pos+2*self.n_columns-1] = np.ravel_multi_index((np.unravel_index(self.state_interfaces[rxn_pos+2*self.n_columns-1], (self.N_spec,self.N_spec))[0],
                                                                                      np.unravel_index(self.state_interfaces[rxn_pos                   ], (self.N_spec,self.N_spec))[1]),
                                                                                      (self.N_spec,self.N_spec))
            # add old interfaces to the count:
            self.interface_counts[self.state_interfaces[rxn_pos+1]] += 1
            self.interface_counts[self.state_interfaces[rxn_pos+2]] += 1
            self.interface_counts[self.state_interfaces[rxn_pos-1]] += 1
            self.interface_counts[self.state_interfaces[rxn_pos-2]] += 1
            self.interface_counts[self.state_interfaces[rxn_pos+2*self.n_columns+1]] += 1
            self.interface_counts[self.state_interfaces[rxn_pos+2*self.n_columns-1]] += 1
        else:
            raise Exception('We must be missing a case....')
        if update_square:
            #old_count = np.bincount(np.ravel(self.state_square), minlength = 20)
            self.state_square_from_interfaces()
            #new_count = np.bincount(np.ravel(self.state_square),minlength = 20)
            #if new_count[7] == old_count[7]+1:
            #    pdb.set_trace()
            #print reaction.R1, reaction.R2, reaction.P1, reaction.P2
            #print self.state_square
        ####don't forget to take this out after debugging--it slows us down
        #if not self.state_square[13:23].all():
        #    pdb.set_trace()

    def through_film_composition(self):
        """
        returns the through film composition of the lattice
        """
        self.state_square_from_interfaces()
        comp = np.zeros((self.n_rows,self.N_spec), dtype=np.float64)
        for row_index, row in enumerate(self.state_square):
            comp[row_index] = np.bincount(row, minlength = self.N_spec)/(float(self.n_columns))
            #pdb.set_trace()
        return comp

    def bulk_composition(self, omit_0 = True):
        """
        Returns the bulk composition fraction.
        if omit_0 is true, it will not include species 0
          (because it is commonly reserved for the vacancy "species"
        """

        self.state_square_from_interfaces()
        spec_count = np.zeros(self.N_spec,dtype=np.float64)
        for row in self.state_square:
            spec_count += np.bincount(row, minlength=self.N_spec)
        if omit_0:
            spec_count[0] = 0
        return spec_count/sum(spec_count)

    def set_diffusion(self, diff_matrix):
        """
        Set the pairwise diffusion propensity matrix
        and create reaction classes for each diffusion
        """
        if diff_matrix.size != self.N_spec**2:
            raise Exception('Diffusion matrix must be have size equal to square of species number')
        if (diff_matrix != diff_matrix.T).all():
            raise Exception('Diffusion matrix must be symmetric')
        for row_index, row in enumerate(diff_matrix):
            for column_index, value in enumerate(row):
                if row_index >= column_index:
                    self.add_reaction(row_index, column_index, 
                                      column_index, row_index, 
                                      value, True)


    class reaction:
        """
        Class that contains all necessary information about a single solid state reaction:
         reactants (R1 and R2 as integers),
        products (P1 and P2  as integers),
        the reaction propensity as a float,
        orientation (True = forced orientation R1-->P1, R2-->P2, false = random)
        """    
        def __init__(self,lattice,
                     R1, R2, P1, P2,
                     propensity,
                     orientation):
            self.R1 = int(R1) 
            self.R2 = int(R2)
            self.P1 = int(P1)
            self.P2 = int(P2)
            self.inter1 = np.ravel_multi_index((R1,R2),(len(lattice.species),len(lattice.species)))
            self.inter2 = np.ravel_multi_index((R2,R1),(len(lattice.species),len(lattice.species)))
            self.pinter1 = np.ravel_multi_index((P1,P2),(len(lattice.species),len(lattice.species)))
            self.pinter2 = np.ravel_multi_index((P2,P1),(len(lattice.species),len(lattice.species)))
            self.propensity = float(propensity)
            self.orientation = bool(orientation)
    
    def add_reaction(self, R1, R2, P1, P2, prop, ori):
        """
        method used to add an allowable reaction to the lattice
        """
        self.reactions += [self.reaction(self, 
                                         int(R1), int(R2), 
                                         int(P1), int(P2), 
                                         float(prop),bool(ori))]
    def size_dist(self, species_list = [1,2]):
        """
        Calculate the agglomeration size distribution for each
        of the species in the species_list
        Default [1,2], is for CuIn and CuGa species (assuming default self.species)
        """
        from scipy.ndimage import measurements
        #update to ensure we have current lattice in square state
        self.state_square_from_interfaces()
        dist = []
        aglom_sizes =[]
        for cur_spec in species_list:
            sparse = self.state_square.copy()
            # zero out the values that are not spec
            for i, row in enumerate(sparse):
                for j, value in enumerate(row):
                    if value != cur_spec:
                        sparse[i,j] = 0
                    else:
                        sparse[i,j] = 1
            
            lw,num = measurements.label(sparse)
            aglom_size = measurements.sum(sparse,lw,index=np.arange(lw.max()+1)).astype(np.int)

           # pdb.set_trace()
            bc = np.bincount(aglom_size)
            bc[0] = 0

            dist += [bc]
            aglom_sizes += [aglom_size]
            

        return (dist,aglom_sizes)


