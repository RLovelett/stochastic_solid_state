"""
module to apply diffusion maps manifold learning method to the stochastic simulation
"""

import numpy as np

def overlap(lat1, lat2):
    """
    calculates the overlap function for 2 lattices, which we use as a distance/similarity
    metric for the dmaps algorithm
    """
    if np.size(lat1) != np.size(lat2):
        raise Exception('Lattices must be equal sizes for overlap method')


    vec1 = lat1.reshape(np.array(zip(np.shape(lat1))).prod())
    vec2 = lat2.reshape(np.array(zip(np.shape(lat2))).prod())

    return (vec1!=vec2).sum()

def dmaps_alg(lattices, epsilon = 1):
    """
    takes a list of lattices and finds the weighting matrix used
    by the diffusion maps algorithm
    epsilon is a tuning parameter
    """

    W = np.zeros((len(lattices),len(lattices)))
    for i in range(len(lattices)):
        for j in range(len(lattices)):
            W[i,j] = np.exp(-overlap(lattices[i],lattices[j])**2/epsilon**2)

    D = np.zeros(np.shape(W))
    for i in range(len(W)):
        D[i,i] = W[i].sum()

    A = np.dot(np.linalg.inv(D),W)

    (L,P) = np.linalg.eig(A)

    # Numpy doesn't order eigenvalues by default: here we reorder
    order = L.argsort()[::-1]
    L = L[order]
    P = P[order]

    return (L,P)

def scree(eigenvalues,output='temp.svg'):
    """
    eigenvalues are the first output from dmaps alg
    scree plot plots eigenvalues in descending order
    """
    import matplotlib.pyplot as plt

    fig = plt.figure()
    ax = fig.add_subplot(111)
    ax.plot(eigenvalues, marker = '*',markersize=10)
    ax.yaxis.set_ticks_position('left')
    ax.xaxis.set_ticks_position('bottom')
    ax.set_title("",y=1.05)
    ax.set_ylabel("$\lambda$", labelpad=10)
    ax.set_xlabel("",labelpad=10)
    plt.savefig(output,format='svg')

    return fig, ax

def spec_counts(lattices):
    """
    lattices is the list of lattices to convert to species counts
    """

    counts = []
    for lattice in lattices:
        counts += [np.bincount(lattice.ravel(), minlength=9)]

    return counts

