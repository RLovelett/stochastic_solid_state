"""
Module contains functions (for now one function, but maybe more if
you forget to change the docstring) that plot information from the
simulation results
"""
import numpy as np

def through_film_Ga_pub(lattice,sulfur=False,Ag=False,output = 'temp.svg', marker='o'):
    """
    input a lattice object
    get a through film Ga plot out
    with publication quality
    input a list of lattices and you might get error bars (only if I ever update this to do that..)
    """
    import matplotlib.pyplot as plt
    import numpy as np
    import statsmodels.api as sm
    import pdb
    #plt.style.use('ggplot')


    raw_comp = lattice.through_film_composition()
    # strip rows with vacancies
    vac_free_comp = raw_comp[raw_comp.T[0]==0].copy()
    # make len equal to full rows only (don't count vacancies)
    Ga_rat = np.zeros(len(raw_comp))

    for row_index, row in enumerate(raw_comp):

        if not sulfur and not Ag:
            if (row[1]+row[2]+row[5]+row[6]+.5*row[7]+.5*row[8]) != 0:
                Ga_rat[row_index] = ((row[2] + row[6] + .5*row[8])/
                           (row[1]+row[2]+row[5]+row[6]+.5*row[7]+.5*row[8]))
            else:
                Ga_rat[row_index]=-1
        elif sulfur:
            #if (row[1]+row[2]+row[7]+row[8]+row[9]+row[10]+(row[11]+row[12]+row[13]+row[14])) >= .1:
            if row[0]+row[3]+row[4] <= 0.25:
                print (row[1]+row[2]+row[7]+row[8]+row[9]+row[10]+row[11]+row[12]+row[13]+row[14])

                Ga_rat[row_index] = ((row[2] + row[9] + row[10] + .5*(row[13] + row[14]))/
                        (row[1] + row[2] + row[7] + row[8] + row[9] + row[10] + .5*(row[11] + row[12] + row[13] + row[14])))
            else:
                Ga_rat[row_index]=-1
        elif Ag:
            if row[0]+row[5] <= 0.25:
                Ga_rat[row_index] = ((row[2]+row[4]+row[9]+.5*(row[11]+row[13]))/
                    (row[1]+row[2]+row[3]+row[4]+row[8]+row[9]+.5*(row[10]+row[11]+row[12]+row[13])))
            else:
                Ga_rat[row_index] = -1

    x = np.linspace(0,1, len(Ga_rat[Ga_rat>=0]))
    Ga_fit = sm.nonparametric.lowess(Ga_rat[Ga_rat>=0],x, frac=1./2,delta = 0.01)
    #pdb.set_trace()
    fig = plt.figure()
    plt.xlim(-.05, 1.05)
    plt.ylim(-.05,1.05)
    ax = fig.add_subplot(111)
    ax.plot(x, Ga_rat[Ga_rat>=0], marker, linestyle = 'None',label='Average Ga/(In+Ga)')
    ax.plot(Ga_fit.T[0], Ga_fit.T[1],linewidth=5, alpha = 0.5, label='lowess fit')
    #pdb.set_trace()
    ax.yaxis.set_ticks_position('left')
    ax.xaxis.set_ticks_position('bottom')
    ax.set_title("", y=1.05)
    ax.set_ylabel("Ga/(In+Ga)", labelpad=10)
    ax.set_xlabel("Relative Position",labelpad=10)
    ax.legend()
    #ax.plot(x, Ga_fit, '-')
    plt.savefig(output, format='svg')



    return fig, ax, Ga_rat
def through_film_Ag_pub(lattice,sulfur=False,output = 'temp.svg', marker='o'):
    """
    input a lattice object
    get a through film Ag plot out
    with publication quality
    input a list of lattices and you might get error bars (only if I ever update this to do that..)
    """
    import matplotlib.pyplot as plt
    import numpy as np
    import statsmodels.api as sm
    import pdb
    #plt.style.use('ggplot')


    raw_comp = lattice.through_film_composition()
    # strip rows with vacancies
    vac_free_comp = raw_comp[raw_comp.T[0]==0].copy()
    # make len equal to full rows only (don't count vacancies)
    Ag_rat = np.zeros(len(raw_comp))

    for row_index, row in enumerate(raw_comp):

        if not sulfur:
            if row[0]+row[5] <= 0.25:
                Ag_rat[row_index] = ((row[3]+row[4]+row[7]+.5*(row[12]+row[13]))/
                    (row[1]+row[2]+row[3]+row[4]+row[6]+row[7]+.5*(row[10]+row[11]+row[12]+row[13])))
            else:
                Ag_rat[row_index] = -1

    x = np.linspace(0,1, len(Ag_rat[Ag_rat>=0]))
    Ag_fit = sm.nonparametric.lowess(Ag_rat[Ag_rat>=0],x, frac=1./2,delta = 0.01)
    #pdb.set_trace()
    fig = plt.figure()
    plt.xlim(-.05, 1.05)
    plt.ylim(-.05,1.05)
    ax = fig.add_subplot(111)
    ax.plot(x, Ag_rat[Ag_rat>=0], marker, linestyle = 'None',label='Average Ag/(Cu+Ag)')
    ax.plot(Ag_fit.T[0], Ag_fit.T[1],linewidth=5, alpha = 0.5, label='lowess fit')
    #pdb.set_trace()
    ax.yaxis.set_ticks_position('left')
    ax.xaxis.set_ticks_position('bottom')
    ax.set_title("", y=1.05)
    ax.set_ylabel("Ag/(Cu+Ag)", labelpad=10)
    ax.set_xlabel("Relative Position",labelpad=10)
    ax.legend()
    #ax.plot(x, Ga_fit, '-')
    plt.savefig(output, format='svg')



    return fig, ax, Ag_rat
def through_film_S_pub(lattice,output = 'temp.svg', marker = 'o'):
    """
    input a lattice object
    get a through film Ga plot out
    with publication quality
    input a list of lattices and you might get error bars (only if I ever update this to do that..)
    """
    import matplotlib.pyplot as plt
    import numpy as np
    import statsmodels.api as sm
    import pdb
    #plt.style.use('ggplot')


    raw_comp = lattice.through_film_composition()
    # strip rows with vacancies
    vac_free_comp = raw_comp[raw_comp.T[0]==0].copy()
    # make len equal to full rows only (don't count vacancies)
    S_rat = np.zeros(len(raw_comp))

    for row_index, row in enumerate(raw_comp):
        #pdb.set_trace()
        #if (row[5]+row[6]+row[7]+row[8]+row[9]+row[10]+ row[11]+row[12]+row[13]+row[14]) >=.1:
        if row[0]+row[3]+row[4] <= 0.25:
            print (row[5]+row[6]+row[7]+row[8]+row[9]+row[10]+ row[11]+row[12]+row[13]+row[14])
            S_rat[row_index] = ((row[6] + row[8] + row[10] + row[12] + row[14])/
                           (row[5]+row[6]+row[7]+row[8]+row[9]+row[10]+ row[11]+row[12]+row[13]+row[14]))
        else:
            S_rat[row_index]=-1
    x = np.linspace(0,1, len(S_rat[S_rat>=0]))
    S_fit = sm.nonparametric.lowess(S_rat[S_rat>=0],x, frac=1./2,delta = 0.01)
    #pdb.set_trace()
    fig = plt.figure()
    plt.xlim(-.05, 1.05)
    plt.ylim(-.05,1.05)
    ax = fig.add_subplot(111)
    ax.plot(x, S_rat[S_rat>=0], marker, linestyle = 'None',label='Average S/(Se+S)')
    ax.plot(S_fit.T[0], S_fit.T[1],linewidth=5, alpha = 0.5, label='lowess fit')
    #pdb.set_trace()
    ax.yaxis.set_ticks_position('left')
    ax.xaxis.set_ticks_position('bottom')
    ax.set_title("", y=1.05)
    ax.set_ylabel("S/(Se+S)", labelpad=10)
    ax.set_xlabel("Relative Position",labelpad=10)
    ax.legend()
    #ax.plot(x, Ga_fit, '-')
    plt.savefig(output, format='svg')

    return fig, ax, S_rat

def through_film_bg(lattice,output = 'temp.svg'):
    """
    input a lattice object
    get a through film bg plot out
    with publication quality
    input a list of lattices and you might get error bars (only if I ever update this to do that..)
    based on Baer formula
    """
    import matplotlib
    import matplotlib.pyplot as plt
    import numpy as np
    import statsmodels.api as sm
    import pdb

    (figG, axG, Ga_rat) = through_film_Ga_pub(lattice,sulfur = True)
    (figS, axS, S_rat) = through_film_S_pub(lattice)
    #pdb.set_trace()
    Ga_rat2 = Ga_rat[Ga_rat>=0.]
    S_rat2 = S_rat[S_rat>=0.]
    x = np.linspace(0,1, len(S_rat[S_rat>0]))
    bg = 1. + 0.13*Ga_rat2[0:len(x)]**2 + 0.08*Ga_rat2[0:len(x)]**2*S_rat2+0.13*Ga_rat2[0:len(x)]*S_rat2+0.55*Ga_rat2[0:len(x)]+0.54*S_rat2

    fig, (axG, axS, ax) = plt.subplots(3, sharex=True)
    x = np.linspace(0,1, len(S_rat[S_rat>=0]))
    #Ga_fit = sm.nonparametric.lowess(Ga_rat[Ga_rat>=0],x, frac=1./2,delta = 0.01)
    #pdb.set_trace()
    axG.set_ylim(-.05,1.05)
    axG.set_xlim(-.05,1.05)
    axG.plot(x, Ga_rat[S_rat>=0], marker = 'o', linestyle = 'None',label='Average Ga/(In+Ga)')
    #axG.plot(Ga_fit.T[0], Ga_fit.T[1],linewidth=5, alpha = 0.5, label='lowess fit')
    #pdb.set_trace()
    axG.yaxis.set_ticks_position('left')
    axG.xaxis.set_ticks_position('bottom')
    axG.set_ylabel("Ga/(In+Ga)", labelpad=10, fontsize=12)

    axS.plot(x, S_rat[S_rat>=0], marker = 'o', linestyle = 'None',label='Average S/(Se+S)')
    #axS.plot(S_fit.T[0], S_fit.T[1],linewidth=5, alpha = 0.5, label='lowess fit')
    #pdb.set_trace()
    axS.set_ylim(-.05,1.05)
    axS.yaxis.set_ticks_position('left')
    axS.xaxis.set_ticks_position('bottom')
    axS.set_ylabel("S/(Se+S)", labelpad=10, fontsize=12)

    ax.set_ylim(.95,2.05)
    ax.plot(x,bg , marker = 'o', linestyle = 'None')
    #pdb.set_trace()
    ax.yaxis.set_ticks_position('left')
    ax.xaxis.set_ticks_position('bottom')
    ax.set_ylabel("Band Gap [eV]", labelpad=10,fontsize=12)
    ax.set_xlabel("Relative Position",labelpad=10)
    ax.legend()
    #ax.plot(x, Ga_fit, '-')

    #f, axarr = plt.subplots(3, sharex=True)
    #axarr[0] = axG
    #axarr[1] = axS
    #axarr[2] = ax
    fig.subplots_adjust(hspace=0)
    plt.savefig(output, format='svg')

    return fig, bg

def composition_evolution(comp, lattice, output = 'comp.svg'):
    """
    plot evolution of composition vs time.
    comp is the output from simulation.comp_evo_experiment
    lattice is the lattice object (used for the species list attribute)
    """

    import matplotlib.pyplot as plt
    import numpy as np
    import statsmodels.api as sm
    import pdb
    #plt.style.use('ggplot')


    time = comp.T[0]/60 # time in minutes
    #pdb.set_trace()
    fig = plt.figure()
    plt.xlim(0,max(time))
    plt.ylim(0,1)
    ax = fig.add_subplot(111)
    for i,spec in enumerate(lattice.species):
        if spec != 'V':
            ax.plot(time,comp.T[i+1],label=spec)
    ax.yaxis.set_ticks_position('left')
    ax.xaxis.set_ticks_position('bottom')
    ax.set_title("", y=1.05)
    ax.set_ylabel("Fractional Composition", labelpad=10)
    ax.set_xlabel("Relative Position",labelpad=10)
    ax.legend()
    #ax.plot(x, Ga_fit, '-')
    plt.savefig(output, format='svg')

def through_film_Ga(lattice,output = 'temp.html'):
    """
    Will use the output from replicate() to make a plot in bokeh with error bars
    The replicates file is the .npz produced by replicate(). Exactly this file must
    be used, because numpy insists on using a weird format for compressed data.
    but that's okay.
    """
    from bokeh.plotting import output_file, figure, show, save, gridplot, VBox
    import numpy as np
    import statsmodels.api as sm
    import pdb


    output_file(output)

    raw_comp = lattice.through_film_composition()
    # strip rows with vacancies
    vac_free_comp = raw_comp[raw_comp.T[0]==0].copy()
    # make len equal to full rows only (don't count vacancies)
    Ga_rat = np.zeros(len(vac_free_comp))

    for row_index, row in enumerate(vac_free_comp):
      Ga_rat[row_index] = (row[2] + row[6] + .5*row[8]/
                           (row[1]+row[2]+row[5]+row[6]+.5*row[7]+.5*row[8]))
    x = np.linspace(0,1, len(Ga_rat))

    Ga_fit = sm.nonparametric.lowess(Ga_rat,x, frac=1./3)
    TOOLS='pan, wheel_zoom, box_zoom, reset, save'
    #pdb.set_trace()

    plot_config = dict(plot_width=400, plot_height=400, tools=TOOLS,
                       title_text_font_size='18pt',outline_line_color='black',
                       background_fill='#E8DDCB')
    Ga_ratio_handle = figure(title='Gallium Profile', x_axis_label = "Relative Film Depth",
                                 y_axis_label='Ga/(In+Ga)', y_range=[0., 1.],
                                 **plot_config)


    Ga_ratio_handle.circle(x, Ga_rat, color='#00539f')
    #Ga_ratio_handle.segment(x,Ga_rat+Ga_errors.T[0],
    #                            x, Ga_rat-Ga_errors.T[1],
    #                            color='#00539f')
    Ga_ratio_handle.grid.grid_line_color='white'
    Ga_ratio_handle.axis.major_label_text_font_size = "12pt"
    Ga_ratio_handle.axis.axis_label_text_font_size='16pt'
    Ga_ratio_handle.line(x,Ga_fit.T[1], alpha=0.4, color='#ffd200',line_width=15)

    Ga_ratio_handle.circle(x, Ga_rat, color='#003976',legend='Ga/(In+Ga)')
    #Ga_ratio_handle.segment(x,Ga_rat+Ga_errors.T[0],
    #                            x, Ga_rat-Ga_errors.T[1],
    #                            color='#003976')
    Ga_ratio_handle.grid.grid_line_color=None
    Ga_ratio_handle.legend.orientation='top_left'
    Ga_ratio_handle.axis.major_label_text_font_size = "9pt"
    Ga_ratio_handle.axis.axis_label_text_font_size='12pt'



    p = Ga_ratio_handle
    save(p)
    return p


def lattice_map(lattice,output = 'temp.html',save_output=True):
    """
    This will produce a map of the lattice.
    Similar to the view_lattice function in lattice_fcns, but
    better looking, we'll use a bokeh heat map.

    Species:
    0. Vacancy
    1. 2 Se
    2. 2 In
    3. CuIn
    4. CuGa
    5. InSe
    6. GaSe
    7. CuInSe2
    8. CuGaSe2
    9. Mo
    10. CuSe
    11. 2 S
    12. InS
    13. GaS
    14. CuS
    15. CuInS2
    16. CuGaS2


    """

    import bokeh.plotting as bk
    from bokeh.models import HoverTool
    import numpy as np
    import pandas as pd
    import pdb
    from collections import OrderedDict

    TOOLS = ('hover','save','pan','box_zoom')
    if save_output:
        bk.output_file(output)

    # define a dictionary to convert the species number to species name
    num_to_color = {
            0: '#d6d8dd', # 'Vacancy',
            1: '#009e00', # 'CuIn',
            2: '#2ecf2e', # 'CuGa',
            3: '#550000', # '2 Se',
            4: '#ff3939', # 'CuSe',
            5: '#9b0000', # 'InSe',
            6: '#ff6363', # 'GaSe',
            7: '#20096a', # 'CuInSe2',
            8: '#5538b4', # 'CuGaSe2',
            9: 'grey', # 'Mo',
            11: '#ffd300', # '2 S',
            12: '#ffdd39', # 'CuS',
            13: '#9b8000', # 'InS',
            14: '#ffe436', # 'GaS',
            15: '#2b0e88', # 'CuInS2',
            16: '#735ac3'} # 'CuGaS2'}

    num_to_species = {
            0:  'Vacancy',
            1:  'CuIn',
            2:  'CuGa',
            3:  '2 Se',
            4:  'CuSe',
            5:  'InSe',
            6:  'GaSe',
            7:  'CuInSe2',
            8:  'CuGaSe2',
            9:  'Mo',
            10: 'CuSe',
            11: 'S',
            12: 'InS',
            13: 'GaS',
            14: 'CuS',
            15: 'CuInS2',
            16: 'CuGaS2'}

    latframe = pd.DataFrame(lattice)

    x_coor = []
    y_coor = []
    colors = []
    species = []

    for i in latframe.index:
        for j in latframe.columns:
            x_coor.append(j)
            y_coor.append(max(latframe.index)-i)
            #pdb.set_trace()
            colors.append(num_to_color[latframe.at[i,j]])
            species.append(num_to_species[latframe.at[i,j]])

    source = bk.ColumnDataSource(
        data = dict(
            x_coor=x_coor,
            y_coor=y_coor,
            colors=colors,
            species=species
            )
        )
    p = bk.figure(title=None,tools= TOOLS,
                  title_text_font_size='12pt',
                  plot_width=45*len(lattice.T), plot_height=45*len(lattice),
                  x_range = [-.75, len(lattice.T)-.25],
                  y_range = [-.75, len(lattice)-.25],
                  x_axis_type = None, y_axis_type=None,
                  background_fill = '#E8DDCB')

    p.rect('x_coor', 'y_coor', 0.9, 0.9, source=source,
           color='colors', line_color='black',line_alpha = .5,name='start') #name is used to animate in the server

    p.grid.grid_line_color = None
    p.axis.axis_line_color = None
    p.axis.major_tick_line_color = None
    p.axis.minor_tick_line_color = None
    p.outline_line_color = 'black'
    hover = p.select(dict(type=HoverTool))
    hover.snap_to_data=False
    hover.tooltips = OrderedDict([
        ('Species', '@species'),
        ('x,y','@x_coor,@y_coor'),
        ])
    if save_output:
        bk.save(p)

    #return p,source,colors


def size_histogram(lattice, spec_list = [2], save=True):
    """
    lattice object input
    outputs a histogram of the size distribution of agglomerations for
    species in the spec_list (default is 1,2, which is CuIn, CuGa for usual application)
    """
    import matplotlib.pyplot as plt
    import pdb
    import scipy.stats as st
    import scipy.special as sps

    #plt.style.use('ggplot')
    (distributions, aglom_sizes) = lattice.size_dist(species_list=spec_list)
    histograms = []
    for i,aglom_size in enumerate(aglom_sizes):
        #pdb.set_trace()
        fig = plt.figure()
        plt.xlim(.5, aglom_size.max()+.5)
        ax = fig.add_subplot(111)
        bins = np.arange(aglom_size.min(),aglom_size.max()+2,1)-.5
        ax.hist(aglom_size[1:],bins, normed=True,
                label = 'Simulation Results', alpha = 1)
        ax.yaxis.set_ticks_position('left')
        ax.xaxis.set_ticks_position('bottom')
        ax.set_title("", y=1.05)
        ax.set_xlabel("Agglomeration Size", labelpad=10)
        ax.set_ylabel("Probability",labelpad=10)
        plt.xlim(0.5, 20.5)
        plt.ylim(0,1)
        # the p parameter for the geometric pdf
        #pdb.set_trace()
        geom_p = sum(distributions[i].astype(np.float))/sum(aglom_size.astype(np.float))
        #pdb.set_trace()
        domain = np.array(range(max(aglom_size)), dtype = np.float)+1
        Pr = st.geom.pmf(domain, geom_p)
        # the alpha and beta paramters for beta-geom pdf
        import bg_estim as b
        #pdb.set_trace()
        (alpha, beta) = b.BG_MLE(aglom_size)
        Pr_bg = np.zeros(len(domain))
        for j,x in enumerate(domain):
            Pr_bg[j] = sps.beta(alpha+1,x+beta-1)/sps.beta(alpha,beta)
        (r,p) = b.NB_MLE(aglom_size)
        #pdb.set_trace()
        Pr_nb = st.nbinom.pmf(domain,n=r,p=p)
        #pdb.set_trace()
        #moment2 = st.moment(aglom_size[1:], moment=2)
        #moment3 = st.moment(aglom_size[1:], moment=3)
        #moment4 = st.moment(aglom_size[1:], moment=4)
        #ax.text(11,.70,('M2=%.2f\nM3=%.2f\nM4=%.2f' % (moment2, moment3, moment4)))
        #ax.plot(domain, Pr,marker = 'o',
        #        label = ('Geometric PDF (p = %.2f)'%geom_p))
        ax.plot(domain, Pr_bg,marker = 'o',
                label = ('Beta Geometric PDF (alpha = %.2f,beta = %0.2f)'%(alpha,beta)))
        ax.plot(domain, Pr_nb,marker = 'x',
                label = ('Negative binomial PDF (r = %.2f, p = %0.2f)'%(r,p)))
        #pdb.set_trace()
        ax.legend()
        if save:
            plt.savefig(('temp'+str(i)+'.svg'), format='svg')
        histograms+=[ax]
    return histograms

def size_histogram_no_lat_obj(aglom_size, save=True):
    """
    lattice object input
    outputs a histogram of the size distribution of agglomerations for
    species in the spec_list (default is 1,2, which is CuIn, CuGa for usual application)
    """
    import matplotlib.pyplot as plt
    import pdb
    import scipy.stats as st
    import scipy.special as sps
    import numpy as np

    distributions = np.bincount(aglom_size.astype(np.int))
    #plt.style.use('ggplot')
    #pdb.set_trace()
    fig = plt.figure()
    plt.xlim(.5, aglom_size.max()+.5)
    ax = fig.add_subplot(111)
    bins = np.arange(aglom_size.min(),aglom_size.max()+2,1)-.5
    ax.hist(aglom_size[1:],bins, normed=True,
            label = 'Simulation Results', alpha = 1)
    ax.yaxis.set_ticks_position('left')
    ax.xaxis.set_ticks_position('bottom')
    ax.set_title("", y=1.05)
    ax.set_xlabel("Agglomeration Size", labelpad=10)
    ax.set_ylabel("Probability",labelpad=10)
    plt.xlim(0.5, 20.5)
    plt.ylim(0,1)
    # the p parameter for the geometric pdf
    #pdb.set_trace()
    geom_p = sum(distributions.astype(np.float))/sum(aglom_size.astype(np.float))
    #pdb.set_trace()
    domain = np.array(range(int(max(aglom_size))), dtype = np.float)+1
    Pr = st.geom.pmf(domain, geom_p)
    # the alpha and beta paramters for beta-geom pdf
    import bg_estim as b
    #pdb.set_trace()
    #(alpha, beta) = b.BG_MLE2(aglom_size)
    #(alpha_bnb,beta_bnb,r_bnb) = b.BNB_MLE(aglom_size)
    (r_nb,p_nb) = b.NB_MLE(aglom_size)
    Pr_bg = np.zeros(len(domain))
    Pr_bnb = np.zeros(len(domain))
    Pr_nb = np.zeros(len(domain))
    for j,x in enumerate(domain):
#        Pr_bg[j] = sps.beta(alpha+1,x+beta-1)/sps.beta(alpha,beta)
#        Pr_bnb[j] = ((sps.gamma(r_bnb+x-1)*sps.beta(alpha_bnb+r_bnb, beta_bnb+x-1))/
#                     (sps.gamma(x-1+1)*sps.gamma(r_bnb)*sps.beta(alpha_bnb,beta_bnb)))
        Pr_nb[j] = sps.gamma(x-1+r_nb)/(sps.gamma(x)*sps.gamma(r_nb)) * p_nb**(x-1)*(1-p_nb)**r_nb
    pdb.set_trace()
    #moment2 = st.moment(aglom_size[1:], moment=2)
    #moment3 = st.moment(aglom_size[1:], moment=3)
    #moment4 = st.moment(aglom_size[1:], moment=4)
    #ax.text(11,.70,('M2=%.2f\nM3=%.2f\nM4=%.2f' % (moment2, moment3, moment4)))
    #ax.plot(domain, Pr,marker = 'o',
    #        label = ('Geometric PDF (p = %.2f)'%geom_p))
    #ax.plot(domain, Pr_bg,marker = 'o',
    #        label = ('Beta Geometric PDF (alpha = %.2f,beta = %0.2f)'%(alpha,beta)))
    #ax.plot(domain, Pr_bnb,marker = 'o',
    #        label = ('Beta Negative binomial PDF (a = %.2f, b = %.2f, r = %.2f)'%(alpha_bnb,beta_bnb,r_bnb)))
    ax.plot(domain, Pr_nb, marker = 'o',
            label = ('Negative binomial PDF (p = %.4f, r = %.4f)'%(p_nb,r_nb)))
    #pdb.set_trace()
    ax.legend()
    if save:
        plt.savefig(('temp'+'.svg'), format='svg')
    histogram=ax
    return histogram

def chi_sq(agglom_data, p, r):
    """
    chi squared goodness of fit test for the agglomeration data using the neg. binomial
    """

    import pdb
    import scipy.stats as st
    import scipy.special as sps
    import numpy as np

    X = agglom_data-1

    f = np.bincount(X)

    m_bins = 0
    N = 0
    C2 = 0.

    for i in range(len(f)):
        if f[i] >= 5:
            m_bins +=1
            N += f[i]
    print N
    for i in range(m_bins):
        if f[i] >=5:
            phi = len(X)*sps.gamma(i+r)/(sps.gamma(i+1)*sps.gamma(r))*(p**(i)*(1-p)**r)
            C2 += (f[i]-phi)**2/phi
            print i, phi, f[i]
    #pdb.set_trace()
    prob = 1-st.chi2.cdf(C2, df = m_bins-3)

    return prob

