"""
function used to estimate the parameters (alpha and beta)
of the beta-geometric distribution
"""

def BG_MLE(data):

    import numpy as np
    import scipy.special as sps
    import scipy.optimize as spo
    import pdb
    data = np.array(data[1:]).astype(np.float)
    def f(pi, theta, data):
        eqns = np.zeros(2)

        n = len(data)

        eqns[0] = float(n)/pi
        for element in data:
            for r in np.arange(1.,element):
                eqns[0] += -1./(1. - pi + (r-1.)*theta)
                eqns[1] += (r - 1.)/(1. - pi + (r-1.)*theta)
            for r in np.arange(1.,element+1.):
                eqns[1] += -(r - 1.)/(1. + (r-1.)*theta)
        #pdb.set_trace()
        return eqns

    X0 = np.array([0.5, .01667])    
    (X, infodict, ier, msg) = spo.fsolve(lambda x : f(x[0], x[1], data), X0,full_output=True)
    if ier != 1:
        raise Exception(msg)
        print msg
    alpha_hat = X[0]/X[1]
    beta_hat = (1. - X[0])/X[1]

    return (alpha_hat, beta_hat)
def BG_MLE2(data):

    import numpy as np
    import scipy.special as sps
    import scipy.optimize as spo
    import pdb
    data = np.array(data[1:]).astype(np.float)
    data = data-1 # form here includes 0 in the domain
    def f(alpha,beta, data):
        eqns = np.zeros(2)

        n = float(len(data))

        eqns[0] = n*sps.psi(alpha+1) + n*sps.psi(alpha+beta)-n*sps.psi(alpha)
        eqns[1] = n*sps.psi(alpha+beta) + n*sps.psi(beta)
        for element in data:
            eqns[0]+= -sps.psi(element+alpha+beta+1)
            eqns[1]+= sps.psi(element+beta) - sps.psi(element+alpha+beta+1)

        #pdb.set_trace()
        return eqns

    X0 = np.array([0.1, 5])    
    (X, infodict, ier, msg) = spo.fsolve(lambda x : f(x[0], x[1], data), X0,full_output=True)
    if ier != 1:
        raise Exception(msg)
        print msg
    alpha_hat = X[0]
    beta_hat = X[1]

    return (alpha_hat, beta_hat)

def plot_beta(alpha, beta,output = 'beta.svg'):
    """
    plot the beta distribution pdf for shape parameters alpha and beta
    """
    import numpy as np
    import matplotlib.pyplot as plt
    import pdb
    import scipy.stats as spst
    import scipy.special as sps

    p = np.linspace(0,1,100) #geom p paramters
    P = spst.beta.pdf(p,alpha,beta)
    fig = plt.figure()
    plt.xlim(0.,1.0)
    plt.ylim(0.,2.5)
    ax = fig.add_subplot(111)
    ax.plot(p, P)
    #pdb.set_trace()
    ax.yaxis.set_ticks_position('left') 
    ax.xaxis.set_ticks_position('bottom')
    ax.set_title("", y=1.05)
    ax.set_ylabel("Beta Probability Density",labelpad=10)
    ax.set_xlabel("Bernoulli Success Probability",labelpad=10)
    #ax.legend()
    #ax.plot(x, Ga_fit, '-')
    plt.savefig(output, format='svg')


    
    return fig

def NB_MLE(data):

    import numpy as np
    import scipy.special as sps
    import scipy.optimize as spo
    import pdb 
    data = np.array(data[1:]).astype(np.float)
    data = data-1
    def f(r, data):
        eqn = 0.
        n = len(data)
        
        for point in data:
            eqn += sps.digamma(point+r)
        eqn += (-n*sps.digamma(r) + n*np.log(r/(r+sum(data)/n)))
        return eqn

    X0 = .1    
    (X, infodict, ier, msg) = spo.fsolve(lambda x : f(x, data), X0,full_output=True)
    r = X
    p = sum(data)/(len(data)*r+sum(data))
    if ier != 1:
        #raise Exception(msg)
        print msg
    return (r, p)

def BNB_MLE(data):

    import numpy as np
    import scipy.special as sps
    import scipy.optimize as spo
    import pdb 

    data = data -1
    def f(alpha, beta, r, data):

        log_like = 0
        
        for element in data:
            log_like += (np.log(sps.gamma(r+element))+
                         np.log(sps.beta(alpha+r, beta+element))+
                        -np.log(sps.gamma(element+1))+
                        -np.log(sps.gamma(r))+
                        -np.log(sps.beta(alpha,beta)))

        print (alpha, beta, r,log_like)
        return -log_like
    #pdb.set_trace()
    X0 = np.array([0.5, 5., .5])    
    res = spo.minimize(lambda x : f(x[0], x[1], x[2], data), X0, method ='Nelder-Mead',
                       bounds = [(0, None), (0,None), (0,None)])
    if not res.success:
        #raise Exception(res.message)
        print res.message
    #pdb.set_trace()
    alpha = res.x[0]
    beta = res.x[1]
    r = res.x[2]
    return (alpha,beta, r)
