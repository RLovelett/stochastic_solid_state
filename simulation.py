"""
Python class file containing the simulation class.
A simulation object contains higher level information than the lattice class
It will build the lattice class and step it though SSA steps
It contains attributes including:
    n_rows -- rows in the lattice
    n_columns -- columns in the lattice
    species -- list of acceptable species names
    Max_SSA_Steps -- the maximum number of SSA steps before quitting
    sulfur -- bool that sets the species list automatically (will overwrite manual species list if given)
    pickle_on -- if true, it will pickle the lattice in it's latest form (not yet implemented)

"""

import numpy as np
import pdb
import bokeh.plotting as bkp
import pandas as pd
import lattice as l
class simulation:

    def __init__(self,
                 n_rows = 23,
                 n_columns = 10,
                 species = ['V', 'CuIn', 'CuGa','2 Se', 'CuSe',
                            'InSe', 'GaSe', '.5 CuInSe2', '.5 CuGaSe2'],
                 Max_SSA_steps = 100,
                 sulfur = False,
                 Ag = False,
                 pickle_on = True):  # pickling lattice state not yet implemented
        self.n_rows = n_rows
        self.n_columns = n_columns
        if sulfur:
            species = ['V', 'CuIn','CuGa','2 Se', '2 S', 'CuSe', 'CuS','InSe','InS','GaSe','GaS','0.5 CuInSe2', '0.5 CuInS2', '0.5 CuGaSe2', '0.5 CuGaS2']
        if Ag:
            species = ['V', 'CuIn', 'CuGa','AgIn','AgGa','2 Se', 'CuSe', 'AgSe', 'InSe', 'GaSe', '0.5 CuInSe2', '0.5 CuGaSe2', '0.5 AgInSe2', '0.5 AgGaSe2']
        self.species = species
        self.Max_SSA_steps = Max_SSA_steps
        self.pickle_on = True

        self.lat = l.lattice(n_rows,n_columns,species)

    def precursor(self, Ga_rat, prec_rows, Ag_rat = 0, Se_rows = 0):
        """
        build a precursor lattice with the specified precursor ratio and
        number of precursor rows (rest of lattice will be vacancies)
        """
        prec = np.zeros((self.n_rows,self.n_columns), dtype = np.int)

        for row_index, row in enumerate(prec):
            if row_index >= self.n_rows-prec_rows-Se_rows:
                for index, value in enumerate(row):
                    prec[row_index, index] = 3
            if row_index >= self.n_rows-prec_rows:
                for index, value in enumerate(row):
                    probs = np.array([0,
                                     (1-Ga_rat)*(1-Ag_rat),
                                     Ga_rat*(1-Ag_rat),
                                     (1-Ga_rat)*Ag_rat,
                                     Ga_rat*Ag_rat])
                    prec[row_index,index] = np.nonzero(np.random.random_sample()<np.cumsum(probs/probs.sum()))[0][0]
        #print prec
        self.lat.manual_initial_condition(prec)

    def load_with_propensity_adjustment(self,rxn_filename, diffusion_filename, l_factor):
        """
        Adjust the propensities such that physical properties
        (reaction rate constants, sticking coeffs, evap rates, diffusivities)
        are constant for different lattice sizes
        rxn_filename is the reaction list,
        diffusion_filename is the diffusion matrix filename
        l_factor is the ratio of the file prop lattice to the actual lattice length
        rxn and diff props are multiplied by l_factor**2, ads/des by l_factor**5
         (see manuscript for derivation for reason why)
        """
        reactions_mat = np.loadtxt(rxn_filename, delimiter=',',
                                   usecols=(0,1,2,3,4,5,6),dtype=np.float)
        for row in reactions_mat:
            if bool(row[6]):
                self.lat.add_reaction(row[0],
                                      row[1],
                                      row[2],
                                      row[3],l_factor**5*row[4],
                                      bool(row[5]))
            else:
                self.lat.add_reaction(row[0],
                                      row[1],
                                      row[2],
                                      row[3],l_factor**2*row[4],
                                      bool(row[5]))

        diffusion_mat = np.loadtxt(diffusion_filename, delimiter=',',
                                   dtype=np.float)
        self.lat.set_diffusion(l_factor**2*diffusion_mat)



    def diffusion_from_csv(self, filename):
        diffusion_mat = np.loadtxt(filename,delimiter = ',',
                                   dtype=np.float)
        self.lat.set_diffusion(diffusion_mat)
    def reaction_from_csv(self,filename):
        reactions_mat = np.loadtxt(filename,delimiter = ',',
                           usecols = (0,1,2,3,4,5))

        for row in reactions_mat:
            self.lat.add_reaction(row[0],row[1],row[2],row[3],row[4],bool(row[5]))

        #pdb.set_trace()
    def run_simulation(self, max_steps, max_time=3600,
                       exit_if_full = False, pickle_name = 'lattice.pickle',
                       fast_diff=False):
        count = 0
        while count <= max_steps:
            self.lat.SSA_step(fast_diff=fast_diff)
            if self.lat.time > max_time:
                print 'Exit due to Time'
                print self.lat.time
                break
            if exit_if_full and not self.lat.interface_counts[0:9].any():
                print 'exit due to full'
                break
            count+=1
        if count == max_steps+1:
            print 'Exit due to steps'
            print self.lat.time

        self.lat.state_square_from_interfaces()
        #if self.pickle_on:
        #    import pickle as pi
        #    target = open(pickle_name, 'w')
        #    pi.dump(self.lat, pickle_name)
        #    target.close()

    def comp_evo_experiment(self, time_step_min, N_steps, fast_diff=False):
        """
        gathers composition data from the lattice by advancing
        N_steps time_steps (to a total time of time_step*N_steps)
        and gathering composition data at each time_step
        """
        if N_steps % time_step_min != 0:
            raise Exception('N_steps must be divisible by time_step')
        comp = np.zeros((N_steps,self.lat.N_spec+1), dtype=np.float)
        for step_number, row in enumerate(comp):
            if step_number > 0:
                print step_number
                self.run_simulation(100000, fast_diff=False, max_time = time_step_min*step_number)
            comp[step_number] = np.append(np.array([self.lat.time]),self.lat.bulk_composition())
        return comp

    def dmaps_experiments(self, max_steps, max_time, time_step=60):
        """
        gathers lattice data for input to dmaps function
        data is time points and complete lattices
        Default time step is 1 minute
        max_steps = max number of time steps
        max_time = time limit
        time_step = simulation time steps to save
        """
        if max_time % time_step != 0:
            raise Exception('Need time steps to divide evently into max steps')
        lattices = [self.lat.state_square.copy()]
        for i in range(max_time/time_step):
            self.run_simulation(max_steps, max_time = (i+1)*time_step)
            lattices += [self.lat.state_square.copy()]

        return lattices

    No_diff_prec = np.array([[3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3],
                             [1,3,3,1,3,3,1,3,3,1,3,3,1,3,3,3],
                             [3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3],
                             [3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3],
                             [1,3,3,1,3,3,1,3,3,1,3,3,1,3,3,3],
                             [3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3],
                             [3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3],
                             [1,3,3,1,3,3,1,3,3,1,3,3,1,3,3,3],
                             [3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3],
                             [3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3],
                             [1,3,3,1,3,3,1,3,3,1,3,3,1,3,3,3],
                             [3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3],
                             [3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3],
                             [1,3,3,1,3,3,1,3,3,1,3,3,1,3,3,3],
                             [3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3],
                             [3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3],
                             [1,3,3,1,3,3,1,3,3,1,3,3,1,3,3,3],
                             [3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3],
                             [3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3],
                             [1,3,3,1,3,3,1,3,3,1,3,3,1,3,3,3],
                             [3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3],
                             [3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3],
                             [1,3,3,1,3,3,1,3,3,1,3,3,1,3,3,3],
                             [3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3],
                             [3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3],
                             [1,3,3,1,3,3,1,3,3,1,3,3,1,3,3,3],
                             [3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3],
                             [3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3],
                             [1,3,3,1,3,3,1,3,3,1,3,3,1,3,3,3],
                             [3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3],
                             [3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3],
                             [3,1,3,3,1,3,3,1,3,3,1,3,3,1,3,3],
                             [3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3]])

    ABPrec = np.array([[0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0],
                      [0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0],
                      [0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0],
                      [0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0],
                      [0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0],
                      [0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0],
                      [0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0],
                      [0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0],
                      [0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0],
                      [0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0],
                      [1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1],
                      [1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1],
                      [1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1],
                      [1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1],
                      [1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1],
                      [1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1],
                      [1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1],
                      [1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1],
                      [1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1],
                      [1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1]])

    CuInPrec = np.array([[0,0,0,0,0,0,0,0,0,0],
                         [0,0,0,0,0,0,0,0,0,0],
                         [0,0,0,0,0,0,0,0,0,0],
                         [0,0,0,0,0,0,0,0,0,0],
                         [0,0,0,0,0,0,0,0,0,0],
                         [0,0,0,0,0,0,0,0,0,0],
                         [0,0,0,0,0,0,0,0,0,0],
                         [0,0,0,0,0,0,0,0,0,0],
                         [0,0,0,0,0,0,0,0,0,0],
                         [0,0,0,0,0,0,0,0,0,0],
                         [0,0,0,0,0,0,0,0,0,0],
                         [0,0,0,0,0,0,0,0,0,0],
                         [0,0,0,0,0,0,0,0,0,0],
                         [1,1,1,1,1,1,1,1,1,1],
                         [1,1,1,1,1,1,1,1,1,1],
                         [1,1,1,1,1,1,1,1,1,1],
                         [1,1,1,1,1,1,1,1,1,1],
                         [1,1,1,1,1,1,1,1,1,1],
                         [1,1,1,1,1,1,1,1,1,1],
                         [1,1,1,1,1,1,1,1,1,1],
                         [1,1,1,1,1,1,1,1,1,1],
                         [1,1,1,1,1,1,1,1,1,1],
                         [1,1,1,1,1,1,1,1,1,1]])

    CuIn_on_CuGaPrec = np.array([[0,0,0,0,0,0,0,0,0,0],
                         [0,0,0,0,0,0,0,0,0,0],
                         [0,0,0,0,0,0,0,0,0,0],
                         [0,0,0,0,0,0,0,0,0,0],
                         [0,0,0,0,0,0,0,0,0,0],
                         [0,0,0,0,0,0,0,0,0,0],
                         [0,0,0,0,0,0,0,0,0,0],
                         [0,0,0,0,0,0,0,0,0,0],
                         [0,0,0,0,0,0,0,0,0,0],
                         [0,0,0,0,0,0,0,0,0,0],
                         [0,0,0,0,0,0,0,0,0,0],
                         [0,0,0,0,0,0,0,0,0,0],
                         [0,0,0,0,0,0,0,0,0,0],
                         [1,1,1,1,1,1,1,1,1,1],
                         [1,1,1,1,1,1,1,1,1,1],
                         [1,1,1,1,1,1,1,1,1,1],
                         [1,1,1,1,1,1,1,1,1,1],
                         [1,1,1,1,1,1,1,1,1,1],
                         [2,2,2,2,2,2,2,2,2,2],
                         [2,2,2,2,2,2,2,2,2,2],
                         [2,2,2,2,2,2,2,2,2,2],
                         [2,2,2,2,2,2,2,2,2,2],
                         [2,2,2,2,2,2,2,2,2,2]])

