# README #

This software is designed for simulating the deposition of thin solid films including reaction and diffusion occurring within the film. The particular case of Cu(InGa)(SeS)2 deposition is accessible using default function arguments. Details on the algorithm are pending publication and will be posted here when available.

Currently, simulation is *not* Python 3.x compliant and will only work with Python 2.7

### Running a Baseline Reaction ###

Program is most easily accessed using the iPython shell (though the traditional Python shell should work as well). The following commands will run a baseline simulation of Cu(InGa)(SeS)2 thin film growth.

```python
import simulation as s # import the simulation object which has methods to easily configure a simulation

sim = s.simulation.run(n_rows=23, n_columns=100, sulfur=True) # define the geometry; sulfur = true defines the species list;
                                                              #alternatively, the species variable can take a custom species list

sim.precursor(0.25, 10) # a method specifically for Cu(InGa)(SeS) simulation; defines the initial state of the lattice as a 10-row
                        # mixture of CuIn and CuGa species with CuGa/(CuIn+CuGa)=0.25 (remaining rows are vacancies)

sim.load_with_propensity_adjustment('reaction_baseline_with_S.csv',
                                    'diffusion_baseline_with_S.csv', 1) # load the lattice events with propensities for a baseline
                                                                        # reaction with Se and S. The third input (1) will adjust
                                                                        # propensities for different lattice sizes (e.g., if the
                                                                        # simulated lattice size were 2x as large as the lattice
                                                                        # size the propensities were determined for, input 2)

sim.run_simulation(N_max, t_max) # run the simulation; will terminate after N_max steps or t_max simulation time

```

### Source Code Information ###

* lattice.py contains a lattice class and methods for manipulating the lattice object. Importantly, it contains the SSA_step which implements a stochastic simulation algorithm to advance the lattice one time step into the future.
* simulation.py contains a simulation class to easily set simulation properties (such as lattice geometry, number of time steps, allowable species, etc.
* plot_fcns.py contains scripts for producing plots of model outputs
* baseline_reactions.csv, baseline_reactions_with_S.csv contain reaction and adsorption lists and propensities for baseline simulations with Se only and Se/S simulations, respectively.
* baseline_diffusions.csv, baseline_diffusions_with_S.csv contain diffusion propensities for baseline simulations with Se only and Se/S simulations, respectively. 
### Contact Information ###

* Please contact Robert Lovelett (robert.lovelett@gmail.com) with any questions